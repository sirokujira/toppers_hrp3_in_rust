//use std::process::Command;
//use std::env;
//use std::path::Path;

/*
extern crate gcc;

fn main(){
    gcc::Config::new()
                .file("org/foo.c")
                .include("src")
                .compile("libfoo.a");
}
*/

fn main() {
    let out_dir = env::var("OUT_DIR").unwrap();

    let toppers_hrp3_kernel_top = "~/toppers_hrp3_in_rust";

    let inc_dirs = [
        &format!("-I{}/", toppers_hrp3_kernel_top),
        &format!("-I{}/arch/org", toppers_hrp3_kernel_top),
        // &format!("-I{}/include/org", toppers_hrp3_kernel_top),
        &format!("-I{}/include", toppers_hrp3_kernel_top),
        &format!("-I{}/kernel/org", toppers_hrp3_kernel_top),
        &format!("-I{}/library/org", toppers_hrp3_kernel_top),
        // &format!("-I{}/syssvc/org", toppers_hrp3_kernel_top),
        &format!("-I{}/syssvc", toppers_hrp3_kernel_top),
        // mbed
        "-Ikernel"
    ];

    let defines = [
        "-DPLATFORM_ARM",
        "-DALLFUNC",
        "-DTOPPERS_SVC_CALL",
        "-DSUPPORT_XLOG_SYS",
    ];

    let srcs = [
        /* kernel */
        [&format!("{}/kernel/org", toppers_hrp3_kernel_top), "alarm.c"],
        [&format!("{}/kernel/org", toppers_hrp3_kernel_top), "cyclic.c"],
        [&format!("{}/kernel/org", toppers_hrp3_kernel_top), "dataqueue.c"],
        [&format!("{}/kernel/org", toppers_hrp3_kernel_top), "domain.c"],
        [&format!("{}/kernel/org", toppers_hrp3_kernel_top), "eventflag.c"],
        [&format!("{}/kernel/org", toppers_hrp3_kernel_top), "exception.c"],
        [&format!("{}/kernel/org", toppers_hrp3_kernel_top), "interrupt.c"],
        [&format!("{}/kernel/org", toppers_hrp3_kernel_top), "mem_manage.c"],
        [&format!("{}/kernel/org", toppers_hrp3_kernel_top), "memory.c"],
        [&format!("{}/kernel/org", toppers_hrp3_kernel_top), "mempfix.c"],
        [&format!("{}/kernel/org", toppers_hrp3_kernel_top), "messagebuf.c"],
        [&format!("{}/kernel/org", toppers_hrp3_kernel_top), "mutex.c"],
        [&format!("{}/kernel/org", toppers_hrp3_kernel_top), "pridataq.c"],
        [&format!("{}/kernel/org", toppers_hrp3_kernel_top), "semaphore.c"],
        [&format!("{}/kernel/org", toppers_hrp3_kernel_top), "startup.c"],
        [&format!("{}/kernel/org", toppers_hrp3_kernel_top), "svc_table.c"],
        [&format!("{}/kernel/org", toppers_hrp3_kernel_top), "sys_manage.c"],
        [&format!("{}/kernel/org", toppers_hrp3_kernel_top), "task.c"],
        [&format!("{}/kernel/org", toppers_hrp3_kernel_top), "task_manage.c"],
        [&format!("{}/kernel/org", toppers_hrp3_kernel_top), "task_refer.c"],
        [&format!("{}/kernel/org", toppers_hrp3_kernel_top), "task_sync.c"],
        [&format!("{}/kernel/org", toppers_hrp3_kernel_top), "task_term.c"],
        [&format!("{}/kernel/org", toppers_hrp3_kernel_top), "taskhook.c"],
        [&format!("{}/kernel/org", toppers_hrp3_kernel_top), "time_event.c"],
        [&format!("{}/kernel/org", toppers_hrp3_kernel_top), "time_manage.c"],
        [&format!("{}/kernel/org", toppers_hrp3_kernel_top), "wait.c"],

        /* syssvc */
        [&format!("{}/syssvc", toppers_hrp3_kernel_top), "tBannerMain.c"],
        [&format!("{}/syssvc", toppers_hrp3_kernel_top), "tHistogram.c"],
        [&format!("{}/syssvc", toppers_hrp3_kernel_top), "tHistogramAdapter.c"],
        [&format!("{}/syssvc", toppers_hrp3_kernel_top), "tLogTaskMain.c"],
        [&format!("{}/syssvc", toppers_hrp3_kernel_top), "tSerialAdapter.c"],
        [&format!("{}/syssvc", toppers_hrp3_kernel_top), "tSerialPortMain.c"],
        [&format!("{}/syssvc", toppers_hrp3_kernel_top), "tSysLog.c"],
        [&format!("{}/syssvc", toppers_hrp3_kernel_top), "tSysLogAdapter.c"],
        [&format!("{}/syssvc", toppers_hrp3_kernel_top), "tTestService.c"],
        [&format!("{}/syssvc", toppers_hrp3_kernel_top), "tTestServiceAdapter.c"],

        /* library */
        [&format!("{}/library/org", toppers_hrp3_kernel_top), "log_output.c"],
        [&format!("{}/library/org", toppers_hrp3_kernel_top), "prb_str.c"],
        [&format!("{}/library/org", toppers_hrp3_kernel_top), "strerror.c"],
        [&format!("{}/library/org", toppers_hrp3_kernel_top), "t_perror.c"],
        [&format!("{}/library/org", toppers_hrp3_kernel_top), "vasyslog.c"],

        /* mbed */
    ];

    let mut objs: Vec<String> = Vec::new();

    for src in &srcs {
        let obj = src[1].to_string().replace(".c", ".o");

        Command::new("arm-none-eabi-gcc")
            .arg("-c")
            .args(&["-mcpu=cortex-a9", "-mthumb", "-mfloat-abi=soft"])
            .args(&defines)
            .args(&inc_dirs)
            .arg(&format!("{}/{}",src[0], src[1]))
            .arg("-o")
            .arg(&format!("{}/{}", out_dir, obj))
            .status().unwrap();

        objs.push(obj);
    }

    Command::new("arm-none-eabi-as")
        .args(&["-mcpu=cortex-a9", "-mthumb", "-mfloat-abi=soft"])
        .args(&["../../arch/arm_gcc/common/start.S"])
        .args(&["-o"])
        .arg(&format!("{}/start.o", out_dir))
        .status().unwrap();
    objs.push("start.o");

    Command::new("arm-none-eabi-as")
        .args(&["-mcpu=cortex-a9", "-mthumb", "-mfloat-abi=soft"])
        .args(&["../../arch/arm_gcc/common/core_support.S"])
        .args(&["-o"])
        .arg(&format!("{}/core_support.o", out_dir))
        .status().unwrap();
    objs.push("core_support.o");

    Command::new("arm-none-eabi-as")
        .args(&["-mcpu=cortex-a9", "-mthumb", "-mfloat-abi=soft"])
        .args(&["../../arch/arm_gcc/common/gic_support.S"])
        .args(&["-o"])
        .arg(&format!("{}/gic_support.o", out_dir))
        .status().unwrap();
    objs.push("gic_support.o");

    Command::new("arm-none-eabi-ar")
        .args(&["crus", "libkernel.a"])
        .arg(&format!("{}/start.o", out_dir))
        // .arg(&format!("{}/core_support.o", out_dir))
        // .arg(&format!("{}/start.o", out_dir))
        .arg(&format!("{}/*.o", out_dir))
        .current_dir(&Path::new(&out_dir))
        .status().unwrap();

    // link libkernel.a
    println!("cargo:rustc-link-search=native=.");
    println!("cargo:rustc-link-search=native={}", out_dir);
    println!("cargo:rustc-link-lib=static=kernel");

    println!("cargo:rerun-if-changed=build.rs");
    // println!("cargo:rerun-if-changed=target/gr_peach_gcc/rom.ld");
    println!("cargo:rerun-if-changed=../../arch/arm_gcc/common/start.S");
    println!("cargo:rerun-if-changed=../../arch/arm_gcc/common/core_support.S");
    println!("cargo:rerun-if-changed=../../arch/arm_gcc/common/gic_support.S");
}
