//! Core ToppersHRP3 Kernel
//!
//! The kernel crate implements the core features of ToppersHRP3 as well as shared
//! code that many chips, capsules, and boards use. It also holds the Hardware
//! Interface Layer (HIL) definitions.
//!
//! Most `unsafe` code is in this kernel crate.

extern crate libc;

use libc::{ c_void, c_char, size_t };
use std::ffi::{ CStr, CString };

#[link(name = "kernel", kind = "static")]
extern {
  fn initialize_task();
  fn task_terminate();
}

// use rust?
// pub mod callback;
// pub mod component;
// :
