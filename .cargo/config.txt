[build]
target = "armv7a-none-eabi"

[target.armv7s-none-eabi]
rustflags = [
  "-C", "link-arg=-Tcfg1_out.ld",
  "-C", "link-arg=-nostartfiles",
  "-C", "link-arg=-specs=nosys.specs",
  "-C", "link-arg=-specs=nano.specs",
  "-C", "target-feature=-fp-armv7",
  "-C", "target-cpu=cortex-a9",
]

[target.armv7a-none-eabi]
rustflags = [
  "-C", "link-arg=-Tgr_peach_rom_chip.ld",
  "-C", "link-arg=-nostartfiles",
  "-C", "link-arg=-specs=nosys.specs",
  "-C", "link-arg=-specs=nano.specs",
  "-C", "link-arg=-nostdlib",
  "-C", "link-arg=-lstdc++ -lsupc++ -lm -lc -lgcc",
#   "-C", "target-feature=-fp-armv7",
#   "-C", "target-cpu=cortex-a9",
]

[target.aarch64-unknown-none]
rustflags = [
  "-C", "link-arg=-Tlink.ld",
  "-C", "target-feature=-fp-armv8",
  "-C", "target-cpu=cortex-a53",
]