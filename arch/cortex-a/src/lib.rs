//! Generic support for all Cortex-A platforms.

#![crate_name = "cortexa"]
#![crate_type = "rlib"]
#![feature(asm, const_fn, lang_items, used)]
#![no_std]

#[macro_use(register_bitfields, register_bitmasks)]
extern crate kernel;

pub mod nvic;
pub mod scb;
pub mod support;
pub mod syscall;
pub mod systick;
