/*
 * This file was automatically generated by tecsgen.
 * This file is not intended to be edited.
 */
#include "tHRPSVCPlugin_sSerialPortSVCBody_SerialPort1_eSerialPort_tecsgen.h"
#include "tHRPSVCPlugin_sSerialPortSVCBody_SerialPort1_eSerialPort_factory.h"


/* entry port descriptor referenced by call port (differ from actual definition) #_CPEPD_# */

/* call port array #_CPA_# */

/* array of attr/var #_AVAI_# */
/* entry port descriptor #_EPD_# */
/* CB initialize code #_CIC_# */
void
tHRPSVCPlugin_sSerialPortSVCBody_SerialPort1_eSerialPort_CB_initialize()
{
    SET_CB_INIB_POINTER(i,p_cb)
    INITIALIZE_CB()
}
