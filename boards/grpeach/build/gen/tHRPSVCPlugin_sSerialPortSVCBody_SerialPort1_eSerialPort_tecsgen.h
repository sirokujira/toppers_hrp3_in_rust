/*
 * This file was automatically generated by tecsgen.
 * This file is not intended to be edited.
 */
#ifndef tHRPSVCPlugin_sSerialPortSVCBody_SerialPort1_eSerialPort_TECSGEN_H
#define tHRPSVCPlugin_sSerialPortSVCBody_SerialPort1_eSerialPort_TECSGEN_H

/*
 * celltype          :  tHRPSVCPlugin_sSerialPortSVCBody_SerialPort1_eSerialPort
 * global name       :  tHRPSVCPlugin_sSerialPortSVCBody_SerialPort1_eSerialPort
 * idx_is_id(actual) :  no(no)
 * singleton         :  yes
 * has_CB            :  false
 * has_INIB          :  false
 * rom               :  yes
 * CB initializer    :  yes
 */

/* global header #_IGH_# */
#include "global_tecsgen.h"

/* signature header #_ISH_# */
#include "sSerialPort_tecsgen.h"

#ifndef TOPPERS_MACRO_ONLY

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */
/* cell CB (dummy) type definition #_CCDP_# */
typedef struct tag_tHRPSVCPlugin_sSerialPortSVCBody_SerialPort1_eSerialPort_CB {
    int  dummy;
} tHRPSVCPlugin_sSerialPortSVCBody_SerialPort1_eSerialPort_CB;
/* singleton cell CB prototype declaration #_SCP_# */


/* celltype IDX type #_CTIX_# */
typedef int   tHRPSVCPlugin_sSerialPortSVCBody_SerialPort1_eSerialPort_IDX;
#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* TOPPERS_MACRO_ONLY */

/* to get the definition of CB type of referenced celltype for optimization #_ICT_# */
#ifndef  TOPPERS_CB_TYPE_ONLY
#define  tHRPSVCPlugin_sSerialPortSVCBody_SerialPort1_eSerialPort_CB_TYPE_ONLY
#define TOPPERS_CB_TYPE_ONLY
#endif  /* TOPPERS_CB_TYPE_ONLY */
#include "tSerialPortMain_tecsgen.h"
#ifdef  tHRPSVCPlugin_sSerialPortSVCBody_SerialPort1_eSerialPort_CB_TYPE_ONLY
#undef TOPPERS_CB_TYPE_ONLY
#endif /* tHRPSVCPlugin_sSerialPortSVCBody_SerialPort1_eSerialPort_CB_TYPE_ONLY */
#ifndef TOPPERS_CB_TYPE_ONLY


/* celll CB macro #_GCB_# */
#define tHRPSVCPlugin_sSerialPortSVCBody_SerialPort1_eSerialPort_GET_CELLCB(idx) ((void *)0)
 /* call port function macro #_CPM_# */
#define tHRPSVCPlugin_sSerialPortSVCBody_SerialPort1_eSerialPort_cCall_open( ) \
	  tSerialPortMain_eSerialPort_open( \
	   &tSerialPortMain_CB_tab[0] )
#define tHRPSVCPlugin_sSerialPortSVCBody_SerialPort1_eSerialPort_cCall_close( ) \
	  tSerialPortMain_eSerialPort_close( \
	   &tSerialPortMain_CB_tab[0] )
#define tHRPSVCPlugin_sSerialPortSVCBody_SerialPort1_eSerialPort_cCall_read( buffer, length ) \
	  tSerialPortMain_eSerialPort_read( \
	   &tSerialPortMain_CB_tab[0], (buffer), (length) )
#define tHRPSVCPlugin_sSerialPortSVCBody_SerialPort1_eSerialPort_cCall_write( buffer, length ) \
	  tSerialPortMain_eSerialPort_write( \
	   &tSerialPortMain_CB_tab[0], (buffer), (length) )
#define tHRPSVCPlugin_sSerialPortSVCBody_SerialPort1_eSerialPort_cCall_control( ioControl ) \
	  tSerialPortMain_eSerialPort_control( \
	   &tSerialPortMain_CB_tab[0], (ioControl) )
#define tHRPSVCPlugin_sSerialPortSVCBody_SerialPort1_eSerialPort_cCall_refer( pk_rpor ) \
	  tSerialPortMain_eSerialPort_refer( \
	   &tSerialPortMain_CB_tab[0], (pk_rpor) )

#endif /* TOPPERS_CB_TYPE_ONLY */

#ifndef TOPPERS_MACRO_ONLY

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#ifndef TOPPERS_CB_TYPE_ONLY

#endif /* TOPPERS_CB_TYPE_ONLY */

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* TOPPERS_MACRO_ONLY */

#ifndef TOPPERS_CB_TYPE_ONLY


/* cell CB macro (abbrev) #_GCBA_# */
#define GET_CELLCB(idx)  tHRPSVCPlugin_sSerialPortSVCBody_SerialPort1_eSerialPort_GET_CELLCB(idx)

/* CELLCB type (abbrev) #_CCT_# */
#define CELLCB	tHRPSVCPlugin_sSerialPortSVCBody_SerialPort1_eSerialPort_CB

/* celltype IDX type (abbrev) #_CTIXA_# */
#define CELLIDX	tHRPSVCPlugin_sSerialPortSVCBody_SerialPort1_eSerialPort_IDX

/* call port function macro (abbrev) #_CPMA_# */
#define cCall_open( ) \
          tHRPSVCPlugin_sSerialPortSVCBody_SerialPort1_eSerialPort_cCall_open( )
#define cCall_close( ) \
          tHRPSVCPlugin_sSerialPortSVCBody_SerialPort1_eSerialPort_cCall_close( )
#define cCall_read( buffer, length ) \
          tHRPSVCPlugin_sSerialPortSVCBody_SerialPort1_eSerialPort_cCall_read( buffer, length )
#define cCall_write( buffer, length ) \
          tHRPSVCPlugin_sSerialPortSVCBody_SerialPort1_eSerialPort_cCall_write( buffer, length )
#define cCall_control( ioControl ) \
          tHRPSVCPlugin_sSerialPortSVCBody_SerialPort1_eSerialPort_cCall_control( ioControl )
#define cCall_refer( pk_rpor ) \
          tHRPSVCPlugin_sSerialPortSVCBody_SerialPort1_eSerialPort_cCall_refer( pk_rpor )



/* CB initialize macro #_CIM_# */
#define INITIALIZE_CB()
#define SET_CB_INIB_POINTER(i,p_that)\
	/* empty */
#endif /* TOPPERS_CB_TYPE_ONLY */

#include "kernel_cfg.h"
#ifndef TOPPERS_MACRO_ONLY

#endif /* TOPPERS_MACRO_ONLY */

#endif /* tHRPSVCPlugin_sSerialPortSVCBody_SerialPort1_eSerialPort_TECSGENH */
