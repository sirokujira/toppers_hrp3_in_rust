/*
 * This file was automatically generated by tecsgen.
 * This file is not intended to be edited.
 */
#include "tSerialPortMain_tecsgen.h"
#include "tSerialPortMain_factory.h"

/* entry port descriptor type #_EDT_# */
/* eSerialPort : omitted by entry port optimize */

/* enSerialPortManage : omitted by entry port optimize */

/* eiSIOCBR : omitted by entry port optimize */

/* entry port skelton function #_EPSF_# */
/* eSerialPort : omitted by entry port optimize */
/* enSerialPortManage : omitted by entry port optimize */
/* eiSIOCBR : omitted by entry port optimize */

/* entry port skelton function table #_EPSFT_# */
/* eSerialPort : omitted by entry port optimize */
/* enSerialPortManage : omitted by entry port optimize */
/* eiSIOCBR : omitted by entry port optimize */

/* entry port descriptor referenced by call port (differ from actual definition) #_CPEPD_# */

/* call port array #_CPA_# */

/* array of attr/var #_AVAI_# */
char tSerialPortMain_SerialPort1_SerialPortMain_CB_receiveBuffer_INIT[256];
char tSerialPortMain_SerialPort1_SerialPortMain_CB_sendBuffer_INIT[256];
/* cell INIB #_INIB_# */
tSerialPortMain_INIB tSerialPortMain_INIB_tab[] = {
    /* cell: tSerialPortMain_CB_tab[0]:  SerialPort1_SerialPortMain id=1 */
    {
        /* entry port #_EP_# */ 
        /* attribute(RO) */ 
        256,                                     /* receiveBufferSize */
        256,                                     /* sendBufferSize */
        tSerialPortMain_SerialPort1_SerialPortMain_CB_receiveBuffer_INIT, /* receiveBuffer */
        tSerialPortMain_SerialPort1_SerialPortMain_CB_sendBuffer_INIT, /* sendBuffer */
    },
};

/* cell CB #_CB_# */
struct tag_tSerialPortMain_CB tSerialPortMain_CB_tab[1];
/* entry port descriptor #_EPD_# */
/* eSerialPort : omitted by entry port optimize */
/* enSerialPortManage : omitted by entry port optimize */
/* eiSIOCBR : omitted by entry port optimize */
/* CB initialize code #_CIC_# */
void
tSerialPortMain_CB_initialize()
{
    tSerialPortMain_CB	*p_cb;
    int		i;
    FOREACH_CELL(i,p_cb)
        SET_CB_INIB_POINTER(i,p_cb)
        INITIALIZE_CB(p_cb)
    END_FOREACH_CELL
}
