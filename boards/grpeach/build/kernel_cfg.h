/* kernel_cfg.h */
#ifndef TOPPERS_KERNEL_CFG_H
#define TOPPERS_KERNEL_CFG_H

#define TNUM_TSKID	7
#define TSKID_tTask_rKernelDomain_MainTask	1
#define TSKID_tTask_rKernelDomain_ExceptionTask	2
#define TSKID_tTask_rKernelDomain_LogTask_Task	3
#define TSKID_tTask_rDomain1_Task1	4
#define TSKID_tTask_rDomain1_Task2	5
#define TSKID_tTask_rDomain1_AlarmTask	6
#define TSKID_tTask_rDomain2_Task3	7

#define TNUM_SEMID	2
#define SEMID_tSemaphore_rKernelDomain_SerialPort1_ReceiveSemaphore	1
#define SEMID_tSemaphore_rKernelDomain_SerialPort1_SendSemaphore	2

#define TNUM_FLGID	0

#define TNUM_DTQID	0

#define TNUM_PDQID	0

#define TNUM_MTXID	0

#define TNUM_MBFID	0

#define TNUM_MPFID	0

#define TNUM_CYCID	1
#define CYCID_tCyclicHandler_CyclicHandler	1

#define TNUM_ALMID	1
#define ALMID_tAlarmNotifier_rDomain1_AlarmNotifier	1

#define TNUM_ISRID	3
#define ISRID_tISR_rKernelDomain_InterruptServiceRoutine	1
#define ISRID_tISR_rKernelDomain_SIOPortTarget1_RxISRInstance	2
#define ISRID_tISR_rKernelDomain_SIOPortTarget1_TxISRInstance	3

#define TMAX_FNCD	TFN_TECSGEN_ORIGIN + 10

#define TNUM_DOMID	2
#define rDomain1	1
#define rDomain2	2

#define TNUM_SOMID	3
#define SOM1	1
#define SOM2	2
#define SOM3	3

#endif /* TOPPERS_KERNEL_CFG_H */
