/* cfg1_out.c */
#define TOPPERS_CFG1_OUT
#include "kernel/kernel_int.h"
#include "rza1.h"
#include "target_timer.h"
#include "tHRPSVCPlugin_sSysLogSVCBody_SysLog_eSysLog_factory.h"
#include "tHRPSVCPlugin_sSerialPortSVCBody_SerialPort1_eSerialPort_factory.h"
#include "extsvc_fncode.h"
#include "tTask_tecsgen.h"
#include "tTimeEventHandler_tecsgen.h"
#include "tISR_tecsgen.h"
#include "tCpuExceptionHandler_tecsgen.h"
#include "tInitializeRoutine_tecsgen.h"
#include "tTerminateRoutine_tecsgen.h"
#include "tTimeEventHandler.h"
#include "tTimeEventHandler.h"
#include "tSample2.h"

#ifdef INT64_MAX
  typedef int64_t signed_t;
  typedef uint64_t unsigned_t;
#else
  typedef int32_t signed_t;
  typedef uint32_t unsigned_t;
#endif

#include "target_cfg1_out.h"

const uint32_t TOPPERS_magic_number = 0x12345678;
const uint32_t TOPPERS_sizeof_signed_t = sizeof(signed_t);
const uint32_t TOPPERS_sizeof_intptr_t = sizeof(intptr_t);

const unsigned_t TOPPERS_cfg_CHAR_BIT = (unsigned_t)(CHAR_BIT);
const signed_t TOPPERS_cfg_SCHAR_MAX = (signed_t)(SCHAR_MAX);
const signed_t TOPPERS_cfg_SCHAR_MIN = (signed_t)(SCHAR_MIN);
const unsigned_t TOPPERS_cfg_UCHAR_MAX = (unsigned_t)(UCHAR_MAX);
const signed_t TOPPERS_cfg_CHAR_MAX = (signed_t)(CHAR_MAX);
const signed_t TOPPERS_cfg_CHAR_MIN = (signed_t)(CHAR_MIN);
const signed_t TOPPERS_cfg_SHRT_MAX = (signed_t)(SHRT_MAX);
const signed_t TOPPERS_cfg_SHRT_MIN = (signed_t)(SHRT_MIN);
const unsigned_t TOPPERS_cfg_USHRT_MAX = (unsigned_t)(USHRT_MAX);
const signed_t TOPPERS_cfg_INT_MAX = (signed_t)(INT_MAX);
const signed_t TOPPERS_cfg_INT_MIN = (signed_t)(INT_MIN);
const unsigned_t TOPPERS_cfg_UINT_MAX = (unsigned_t)(UINT_MAX);
const signed_t TOPPERS_cfg_LONG_MAX = (signed_t)(LONG_MAX);
const signed_t TOPPERS_cfg_LONG_MIN = (signed_t)(LONG_MIN);
const unsigned_t TOPPERS_cfg_ULONG_MAX = (unsigned_t)(ULONG_MAX);
#if defined(SIL_ENDIAN_BIG)
const signed_t TOPPERS_cfg_SIL_ENDIAN_BIG = (signed_t)(true);
#endif
#if defined(SIL_ENDIAN_LITTLE)
const signed_t TOPPERS_cfg_SIL_ENDIAN_LITTLE = (signed_t)(true);
#endif
#if defined(TOPPERS_SUPPORT_PROTECT)
const signed_t TOPPERS_cfg_TOPPERS_SUPPORT_PROTECT = (signed_t)(true);
#endif
#if defined(TOPPERS_SUPPORT_ATT_MOD)
const signed_t TOPPERS_cfg_TOPPERS_SUPPORT_ATT_MOD = (signed_t)(true);
#endif
#if defined(TOPPERS_SUPPORT_ATT_PMA)
const signed_t TOPPERS_cfg_TOPPERS_SUPPORT_ATT_PMA = (signed_t)(true);
#endif
#if defined(TOPPERS_ML_AUTO)
const signed_t TOPPERS_cfg_TOPPERS_ML_AUTO = (signed_t)(true);
#endif
#if defined(TOPPERS_ML_MANUAL)
const signed_t TOPPERS_cfg_TOPPERS_ML_MANUAL = (signed_t)(true);
#endif
#if defined(USE_EXTERNAL_ID)
const signed_t TOPPERS_cfg_USE_EXTERNAL_ID = (signed_t)(true);
#endif
const unsigned_t TOPPERS_cfg_TA_NULL = (unsigned_t)(TA_NULL);
const unsigned_t TOPPERS_cfg_TA_ACT = (unsigned_t)(TA_ACT);
const unsigned_t TOPPERS_cfg_TA_NOACTQUE = (unsigned_t)(TA_NOACTQUE);
const unsigned_t TOPPERS_cfg_TA_TPRI = (unsigned_t)(TA_TPRI);
const unsigned_t TOPPERS_cfg_TA_WMUL = (unsigned_t)(TA_WMUL);
const unsigned_t TOPPERS_cfg_TA_CLR = (unsigned_t)(TA_CLR);
const unsigned_t TOPPERS_cfg_TA_CEILING = (unsigned_t)(TA_CEILING);
const unsigned_t TOPPERS_cfg_TA_STA = (unsigned_t)(TA_STA);
const unsigned_t TOPPERS_cfg_TA_NOWRITE = (unsigned_t)(TA_NOWRITE);
const unsigned_t TOPPERS_cfg_TA_NOREAD = (unsigned_t)(TA_NOREAD);
const unsigned_t TOPPERS_cfg_TA_EXEC = (unsigned_t)(TA_EXEC);
const unsigned_t TOPPERS_cfg_TA_MEMINI = (unsigned_t)(TA_MEMINI);
const unsigned_t TOPPERS_cfg_TA_MEMZERO = (unsigned_t)(TA_MEMZERO);
const unsigned_t TOPPERS_cfg_TA_SDATA = (unsigned_t)(TA_SDATA);
const unsigned_t TOPPERS_cfg_TA_UNCACHE = (unsigned_t)(TA_UNCACHE);
const unsigned_t TOPPERS_cfg_TA_IODEV = (unsigned_t)(TA_IODEV);
const unsigned_t TOPPERS_cfg_TA_ATTMEM = (unsigned_t)(TA_ATTMEM);
const unsigned_t TOPPERS_cfg_TA_USTACK = (unsigned_t)(TA_USTACK);
const unsigned_t TOPPERS_cfg_TA_TEXTSEC = (unsigned_t)(TA_TEXTSEC);
const unsigned_t TOPPERS_cfg_TA_RODATASEC = (unsigned_t)(TA_RODATASEC);
const unsigned_t TOPPERS_cfg_TA_DATASEC = (unsigned_t)(TA_DATASEC);
const unsigned_t TOPPERS_cfg_TA_BSSSEC = (unsigned_t)(TA_BSSSEC);
const unsigned_t TOPPERS_cfg_TA_NOINITSEC = (unsigned_t)(TA_NOINITSEC);
const unsigned_t TOPPERS_cfg_TA_LOSEC = (unsigned_t)(TA_LOSEC);
const unsigned_t TOPPERS_cfg_TA_ENAINT = (unsigned_t)(TA_ENAINT);
const unsigned_t TOPPERS_cfg_TA_EDGE = (unsigned_t)(TA_EDGE);
const unsigned_t TOPPERS_cfg_TA_NONKERNEL = (unsigned_t)(TA_NONKERNEL);
const unsigned_t TOPPERS_cfg_TA_INISOM = (unsigned_t)(TA_INISOM);
const unsigned_t TOPPERS_cfg_TNFY_HANDLER = (unsigned_t)(TNFY_HANDLER);
const unsigned_t TOPPERS_cfg_TNFY_SETVAR = (unsigned_t)(TNFY_SETVAR);
const unsigned_t TOPPERS_cfg_TNFY_INCVAR = (unsigned_t)(TNFY_INCVAR);
const unsigned_t TOPPERS_cfg_TNFY_ACTTSK = (unsigned_t)(TNFY_ACTTSK);
const unsigned_t TOPPERS_cfg_TNFY_WUPTSK = (unsigned_t)(TNFY_WUPTSK);
const unsigned_t TOPPERS_cfg_TNFY_SIGSEM = (unsigned_t)(TNFY_SIGSEM);
const unsigned_t TOPPERS_cfg_TNFY_SETFLG = (unsigned_t)(TNFY_SETFLG);
const unsigned_t TOPPERS_cfg_TNFY_SNDDTQ = (unsigned_t)(TNFY_SNDDTQ);
const unsigned_t TOPPERS_cfg_TENFY_SETVAR = (unsigned_t)(TENFY_SETVAR);
const unsigned_t TOPPERS_cfg_TENFY_INCVAR = (unsigned_t)(TENFY_INCVAR);
const unsigned_t TOPPERS_cfg_TENFY_ACTTSK = (unsigned_t)(TENFY_ACTTSK);
const unsigned_t TOPPERS_cfg_TENFY_WUPTSK = (unsigned_t)(TENFY_WUPTSK);
const unsigned_t TOPPERS_cfg_TENFY_SIGSEM = (unsigned_t)(TENFY_SIGSEM);
const unsigned_t TOPPERS_cfg_TENFY_SETFLG = (unsigned_t)(TENFY_SETFLG);
const unsigned_t TOPPERS_cfg_TENFY_SNDDTQ = (unsigned_t)(TENFY_SNDDTQ);
const signed_t TOPPERS_cfg_TDOM_KERNEL = (signed_t)(TDOM_KERNEL);
const signed_t TOPPERS_cfg_TDOM_NONE = (signed_t)(TDOM_NONE);
const unsigned_t TOPPERS_cfg_TACP_KERNEL = (unsigned_t)(TACP_KERNEL);
const unsigned_t TOPPERS_cfg_TACP_SHARED = (unsigned_t)(TACP_SHARED);
const signed_t TOPPERS_cfg_TMIN_TPRI = (signed_t)(TMIN_TPRI);
const signed_t TOPPERS_cfg_TMAX_TPRI = (signed_t)(TMAX_TPRI);
const signed_t TOPPERS_cfg_TMIN_DPRI = (signed_t)(TMIN_DPRI);
const signed_t TOPPERS_cfg_TMAX_DPRI = (signed_t)(TMAX_DPRI);
const signed_t TOPPERS_cfg_TMIN_ISRPRI = (signed_t)(TMIN_ISRPRI);
const signed_t TOPPERS_cfg_TMAX_ISRPRI = (signed_t)(TMAX_ISRPRI);
const unsigned_t TOPPERS_cfg_TBIT_FLGPTN = (unsigned_t)(TBIT_FLGPTN);
const unsigned_t TOPPERS_cfg_TMAX_MAXSEM = (unsigned_t)(TMAX_MAXSEM);
const unsigned_t TOPPERS_cfg_TMAX_RELTIM = (unsigned_t)(TMAX_RELTIM);
const unsigned_t TOPPERS_cfg_TMAX_TWDTIM = (unsigned_t)(TMAX_TWDTIM);
const signed_t TOPPERS_cfg_TMIN_INTPRI = (signed_t)(TMIN_INTPRI);
const signed_t TOPPERS_cfg_TMAX_INTPRI = (signed_t)(TMAX_INTPRI);
const signed_t TOPPERS_cfg_TMIN_DOMID = (signed_t)(TMIN_DOMID);
const signed_t TOPPERS_cfg_TMIN_SOMID = (signed_t)(TMIN_SOMID);
const signed_t TOPPERS_cfg_TMIN_TSKID = (signed_t)(TMIN_TSKID);
const signed_t TOPPERS_cfg_TMIN_SEMID = (signed_t)(TMIN_SEMID);
const signed_t TOPPERS_cfg_TMIN_FLGID = (signed_t)(TMIN_FLGID);
const signed_t TOPPERS_cfg_TMIN_DTQID = (signed_t)(TMIN_DTQID);
const signed_t TOPPERS_cfg_TMIN_PDQID = (signed_t)(TMIN_PDQID);
const signed_t TOPPERS_cfg_TMIN_MTXID = (signed_t)(TMIN_MTXID);
const signed_t TOPPERS_cfg_TMIN_MBFID = (signed_t)(TMIN_MBFID);
const signed_t TOPPERS_cfg_TMIN_MPFID = (signed_t)(TMIN_MPFID);
const signed_t TOPPERS_cfg_TMIN_CYCID = (signed_t)(TMIN_CYCID);
const signed_t TOPPERS_cfg_TMIN_ALMID = (signed_t)(TMIN_ALMID);
#if defined(USE_DOMINICTXB)
const signed_t TOPPERS_cfg_USE_DOMINICTXB = (signed_t)(true);
#endif
#if defined(USE_TSKINICTXB)
const signed_t TOPPERS_cfg_USE_TSKINICTXB = (signed_t)(true);
#endif
#if defined(OMIT_INITIALIZE_INTERRUPT)
const signed_t TOPPERS_cfg_OMIT_INITIALIZE_INTERRUPT = (signed_t)(true);
#endif
#if defined(USE_INHINIB_TABLE)
const signed_t TOPPERS_cfg_USE_INHINIB_TABLE = (signed_t)(true);
#endif
#if defined(USE_INTINIB_TABLE)
const signed_t TOPPERS_cfg_USE_INTINIB_TABLE = (signed_t)(true);
#endif
#if defined(OMIT_INITIALIZE_EXCEPTION)
const signed_t TOPPERS_cfg_OMIT_INITIALIZE_EXCEPTION = (signed_t)(true);
#endif
#if defined(OMIT_STANDARD_MEMINIB)
const signed_t TOPPERS_cfg_OMIT_STANDARD_MEMINIB = (signed_t)(true);
#endif
#if defined(OMIT_IDATA)
const signed_t TOPPERS_cfg_OMIT_IDATA = (signed_t)(true);
#endif
#if defined(OMIT_CHECK_USTACK_OVERLAP)
const signed_t TOPPERS_cfg_OMIT_CHECK_USTACK_OVERLAP = (signed_t)(true);
#endif
#if defined(OMIT_STANDARD_DATASECINIB)
const signed_t TOPPERS_cfg_OMIT_STANDARD_DATASECINIB = (signed_t)(true);
#endif
#if defined(OMIT_STANDARD_BSSSECINIB)
const signed_t TOPPERS_cfg_OMIT_STANDARD_BSSSECINIB = (signed_t)(true);
#endif
#if defined(USE_CFG_PASS3)
const signed_t TOPPERS_cfg_USE_CFG_PASS3 = (signed_t)(true);
#endif
#if defined(USE_LATERPASS_DOMINIB)
const signed_t TOPPERS_cfg_USE_LATERPASS_DOMINIB = (signed_t)(true);
#endif
#if defined(USE_REDZONE)
const signed_t TOPPERS_cfg_USE_REDZONE = (signed_t)(true);
#endif
const unsigned_t TOPPERS_cfg_DEFAULT_SSTKSZ = (unsigned_t)(DEFAULT_SSTKSZ);
#if defined(DEFAULT_ISTK)
const uintptr_t TOPPERS_cfg_DEFAULT_ISTK = (uintptr_t)(DEFAULT_ISTK);
#endif
#if defined(TARGET_TSKATR)
const unsigned_t TOPPERS_cfg_TARGET_TSKATR = (unsigned_t)(TARGET_TSKATR);
#else
const unsigned_t TOPPERS_cfg_TARGET_TSKATR = (unsigned_t)(0);
#endif
#if defined(TARGET_INTATR)
const unsigned_t TOPPERS_cfg_TARGET_INTATR = (unsigned_t)(TARGET_INTATR);
#else
const unsigned_t TOPPERS_cfg_TARGET_INTATR = (unsigned_t)(0);
#endif
#if defined(TARGET_INHATR)
const unsigned_t TOPPERS_cfg_TARGET_INHATR = (unsigned_t)(TARGET_INHATR);
#else
const unsigned_t TOPPERS_cfg_TARGET_INHATR = (unsigned_t)(0);
#endif
#if defined(TARGET_ISRATR)
const unsigned_t TOPPERS_cfg_TARGET_ISRATR = (unsigned_t)(TARGET_ISRATR);
#else
const unsigned_t TOPPERS_cfg_TARGET_ISRATR = (unsigned_t)(0);
#endif
#if defined(TARGET_EXCATR)
const unsigned_t TOPPERS_cfg_TARGET_EXCATR = (unsigned_t)(TARGET_EXCATR);
#else
const unsigned_t TOPPERS_cfg_TARGET_EXCATR = (unsigned_t)(0);
#endif
#if defined(TARGET_SVCATR)
const unsigned_t TOPPERS_cfg_TARGET_SVCATR = (unsigned_t)(TARGET_SVCATR);
#else
const unsigned_t TOPPERS_cfg_TARGET_SVCATR = (unsigned_t)(0);
#endif
#if defined(TARGET_REGATR)
const unsigned_t TOPPERS_cfg_TARGET_REGATR = (unsigned_t)(TARGET_REGATR);
#else
const unsigned_t TOPPERS_cfg_TARGET_REGATR = (unsigned_t)(0);
#endif
#if defined(TARGET_MEMATR)
const unsigned_t TOPPERS_cfg_TARGET_MEMATR = (unsigned_t)(TARGET_MEMATR);
#else
const unsigned_t TOPPERS_cfg_TARGET_MEMATR = (unsigned_t)(0);
#endif
#if defined(TARGET_ACCATR)
const unsigned_t TOPPERS_cfg_TARGET_ACCATR = (unsigned_t)(TARGET_ACCATR);
#else
const unsigned_t TOPPERS_cfg_TARGET_ACCATR = (unsigned_t)(0);
#endif
#if defined(TARGET_MIN_SSTKSZ)
const unsigned_t TOPPERS_cfg_TARGET_MIN_SSTKSZ = (unsigned_t)(TARGET_MIN_SSTKSZ);
#else
const unsigned_t TOPPERS_cfg_TARGET_MIN_SSTKSZ = (unsigned_t)(1);
#endif
#if defined(TARGET_MIN_USTKSZ)
const unsigned_t TOPPERS_cfg_TARGET_MIN_USTKSZ = (unsigned_t)(TARGET_MIN_USTKSZ);
#else
const unsigned_t TOPPERS_cfg_TARGET_MIN_USTKSZ = (unsigned_t)(1);
#endif
#if defined(TARGET_MIN_ISTKSZ)
const unsigned_t TOPPERS_cfg_TARGET_MIN_ISTKSZ = (unsigned_t)(TARGET_MIN_ISTKSZ);
#else
const unsigned_t TOPPERS_cfg_TARGET_MIN_ISTKSZ = (unsigned_t)(1);
#endif
#if defined(TARGET_DUMMY_STKSZ)
const unsigned_t TOPPERS_cfg_TARGET_DUMMY_STKSZ = (unsigned_t)(TARGET_DUMMY_STKSZ);
#endif
#if defined(TARGET_MEMATR_USTACK)
const unsigned_t TOPPERS_cfg_TARGET_MEMATR_USTACK = (unsigned_t)(TARGET_MEMATR_USTACK);
#endif
#if defined(TARGET_MEMATR_MPFAREA)
const unsigned_t TOPPERS_cfg_TARGET_MEMATR_MPFAREA = (unsigned_t)(TARGET_MEMATR_MPFAREA);
#endif
#if defined(CHECK_SSTKSZ_ALIGN)
const unsigned_t TOPPERS_cfg_CHECK_SSTKSZ_ALIGN = (unsigned_t)(CHECK_SSTKSZ_ALIGN);
#else
const unsigned_t TOPPERS_cfg_CHECK_SSTKSZ_ALIGN = (unsigned_t)(1);
#endif
#if defined(CHECK_USTKSZ_ALIGN)
const unsigned_t TOPPERS_cfg_CHECK_USTKSZ_ALIGN = (unsigned_t)(CHECK_USTKSZ_ALIGN);
#else
const unsigned_t TOPPERS_cfg_CHECK_USTKSZ_ALIGN = (unsigned_t)(1);
#endif
#if defined(CHECK_INTPTR_ALIGN)
const unsigned_t TOPPERS_cfg_CHECK_INTPTR_ALIGN = (unsigned_t)(CHECK_INTPTR_ALIGN);
#else
const unsigned_t TOPPERS_cfg_CHECK_INTPTR_ALIGN = (unsigned_t)(1);
#endif
#if defined(CHECK_INTPTR_NONNULL)
const signed_t TOPPERS_cfg_CHECK_INTPTR_NONNULL = (signed_t)(true);
#endif
#if defined(CHECK_FUNC_ALIGN)
const unsigned_t TOPPERS_cfg_CHECK_FUNC_ALIGN = (unsigned_t)(CHECK_FUNC_ALIGN);
#else
const unsigned_t TOPPERS_cfg_CHECK_FUNC_ALIGN = (unsigned_t)(1);
#endif
#if defined(CHECK_FUNC_NONNULL)
const signed_t TOPPERS_cfg_CHECK_FUNC_NONNULL = (signed_t)(true);
#endif
#if defined(CHECK_SSTACK_ALIGN)
const unsigned_t TOPPERS_cfg_CHECK_SSTACK_ALIGN = (unsigned_t)(CHECK_SSTACK_ALIGN);
#else
const unsigned_t TOPPERS_cfg_CHECK_SSTACK_ALIGN = (unsigned_t)(1);
#endif
#if defined(CHECK_SSTACK_NONNULL)
const signed_t TOPPERS_cfg_CHECK_SSTACK_NONNULL = (signed_t)(true);
#endif
#if defined(CHECK_USTACK_ALIGN)
const unsigned_t TOPPERS_cfg_CHECK_USTACK_ALIGN = (unsigned_t)(CHECK_USTACK_ALIGN);
#else
const unsigned_t TOPPERS_cfg_CHECK_USTACK_ALIGN = (unsigned_t)(1);
#endif
#if defined(CHECK_USTACK_NONNULL)
const signed_t TOPPERS_cfg_CHECK_USTACK_NONNULL = (signed_t)(true);
#endif
#if defined(CHECK_MPF_ALIGN)
const unsigned_t TOPPERS_cfg_CHECK_MPF_ALIGN = (unsigned_t)(CHECK_MPF_ALIGN);
#else
const unsigned_t TOPPERS_cfg_CHECK_MPF_ALIGN = (unsigned_t)(1);
#endif
#if defined(CHECK_MPF_NONNULL)
const signed_t TOPPERS_cfg_CHECK_MPF_NONNULL = (signed_t)(true);
#endif
const unsigned_t TOPPERS_cfg_sizeof_void_ptr = (unsigned_t)(sizeof(void*));
const unsigned_t TOPPERS_cfg_sizeof_uint_t = (unsigned_t)(sizeof(uint_t));
const unsigned_t TOPPERS_cfg_sizeof_size_t = (unsigned_t)(sizeof(size_t));
const unsigned_t TOPPERS_cfg_sizeof_intptr_t = (unsigned_t)(sizeof(intptr_t));
const unsigned_t TOPPERS_cfg_sizeof_ID = (unsigned_t)(sizeof(ID));
const unsigned_t TOPPERS_cfg_sizeof_FP = (unsigned_t)(sizeof(FP));
const unsigned_t TOPPERS_cfg_sizeof_ACPTN = (unsigned_t)(sizeof(ACPTN));
const unsigned_t TOPPERS_cfg_sizeof_ACVCT = (unsigned_t)(sizeof(ACVCT));
const unsigned_t TOPPERS_cfg_offsetof_ACVCT_acptn1 = (unsigned_t)(offsetof(ACVCT,acptn1));
const unsigned_t TOPPERS_cfg_offsetof_ACVCT_acptn2 = (unsigned_t)(offsetof(ACVCT,acptn2));
const unsigned_t TOPPERS_cfg_offsetof_ACVCT_acptn3 = (unsigned_t)(offsetof(ACVCT,acptn3));
const unsigned_t TOPPERS_cfg_offsetof_ACVCT_acptn4 = (unsigned_t)(offsetof(ACVCT,acptn4));
const unsigned_t TOPPERS_cfg_sizeof_INTNO = (unsigned_t)(sizeof(INTNO));
const unsigned_t TOPPERS_cfg_sizeof_INHNO = (unsigned_t)(sizeof(INHNO));
const unsigned_t TOPPERS_cfg_sizeof_EXCNO = (unsigned_t)(sizeof(EXCNO));
const unsigned_t TOPPERS_cfg_sizeof_TASK = (unsigned_t)(sizeof(TASK));
const unsigned_t TOPPERS_cfg_sizeof_TMEHDR = (unsigned_t)(sizeof(TMEHDR));
const unsigned_t TOPPERS_cfg_sizeof_ISR = (unsigned_t)(sizeof(ISR));
const unsigned_t TOPPERS_cfg_sizeof_INTHDR = (unsigned_t)(sizeof(INTHDR));
const unsigned_t TOPPERS_cfg_sizeof_EXCHDR = (unsigned_t)(sizeof(EXCHDR));
const unsigned_t TOPPERS_cfg_sizeof_EXTSVC = (unsigned_t)(sizeof(EXTSVC));
const unsigned_t TOPPERS_cfg_sizeof_INIRTN = (unsigned_t)(sizeof(INIRTN));
const unsigned_t TOPPERS_cfg_sizeof_TERRTN = (unsigned_t)(sizeof(TERRTN));
const unsigned_t TOPPERS_cfg_sizeof_NFYHDR = (unsigned_t)(sizeof(NFYHDR));
const unsigned_t TOPPERS_cfg_sizeof_TWDINIB = (unsigned_t)(sizeof(TWDINIB));
const unsigned_t TOPPERS_cfg_offsetof_TWDINIB_twdlen = (unsigned_t)(offsetof(TWDINIB,twdlen));
const unsigned_t TOPPERS_cfg_offsetof_TWDINIB_p_dominib = (unsigned_t)(offsetof(TWDINIB,p_dominib));
const unsigned_t TOPPERS_cfg_sizeof_SOMINIB = (unsigned_t)(sizeof(SOMINIB));
const unsigned_t TOPPERS_cfg_offsetof_SOMINIB_p_twdinib = (unsigned_t)(offsetof(SOMINIB,p_twdinib));
const unsigned_t TOPPERS_cfg_offsetof_SOMINIB_p_nxtsom = (unsigned_t)(offsetof(SOMINIB,_kernel_p_nxtsom));
const unsigned_t TOPPERS_cfg_sizeof_DOMINIB = (unsigned_t)(sizeof(DOMINIB));
const unsigned_t TOPPERS_cfg_offsetof_DOMINIB_domptn = (unsigned_t)(offsetof(DOMINIB,domptn));
const unsigned_t TOPPERS_cfg_sizeof_TINIB = (unsigned_t)(sizeof(TINIB));
const unsigned_t TOPPERS_cfg_offsetof_TINIB_domid = (unsigned_t)(offsetof(TINIB,domid));
const unsigned_t TOPPERS_cfg_offsetof_TINIB_tskatr = (unsigned_t)(offsetof(TINIB,tskatr));
const unsigned_t TOPPERS_cfg_offsetof_TINIB_exinf = (unsigned_t)(offsetof(TINIB,exinf));
const unsigned_t TOPPERS_cfg_offsetof_TINIB_task = (unsigned_t)(offsetof(TINIB,task));
const unsigned_t TOPPERS_cfg_offsetof_TINIB_ipriority = (unsigned_t)(offsetof(TINIB,ipriority));
#if !defined(USE_TSKINICTXB)
const unsigned_t TOPPERS_cfg_offsetof_TINIB_sstksz = (unsigned_t)(offsetof(TINIB,sstksz));
#endif
#if !defined(USE_TSKINICTXB)
const unsigned_t TOPPERS_cfg_offsetof_TINIB_sstk = (unsigned_t)(offsetof(TINIB,sstk));
#endif
#if !defined(USE_TSKINICTXB)
const unsigned_t TOPPERS_cfg_offsetof_TINIB_ustksz = (unsigned_t)(offsetof(TINIB,ustksz));
#endif
#if !defined(USE_TSKINICTXB)
const unsigned_t TOPPERS_cfg_offsetof_TINIB_ustk = (unsigned_t)(offsetof(TINIB,ustk));
#endif
const unsigned_t TOPPERS_cfg_offsetof_TINIB_acvct = (unsigned_t)(offsetof(TINIB,acvct));
const unsigned_t TOPPERS_cfg_sizeof_SEMINIB = (unsigned_t)(sizeof(SEMINIB));
const unsigned_t TOPPERS_cfg_offsetof_SEMINIB_sematr = (unsigned_t)(offsetof(SEMINIB,sematr));
const unsigned_t TOPPERS_cfg_offsetof_SEMINIB_isemcnt = (unsigned_t)(offsetof(SEMINIB,isemcnt));
const unsigned_t TOPPERS_cfg_offsetof_SEMINIB_maxsem = (unsigned_t)(offsetof(SEMINIB,maxsem));
const unsigned_t TOPPERS_cfg_offsetof_SEMINIB_acvct = (unsigned_t)(offsetof(SEMINIB,acvct));
const unsigned_t TOPPERS_cfg_sizeof_FLGPTN = (unsigned_t)(sizeof(FLGPTN));
const unsigned_t TOPPERS_cfg_sizeof_FLGINIB = (unsigned_t)(sizeof(FLGINIB));
const unsigned_t TOPPERS_cfg_offsetof_FLGINIB_flgatr = (unsigned_t)(offsetof(FLGINIB,flgatr));
const unsigned_t TOPPERS_cfg_offsetof_FLGINIB_iflgptn = (unsigned_t)(offsetof(FLGINIB,iflgptn));
const unsigned_t TOPPERS_cfg_offsetof_FLGINIB_acvct = (unsigned_t)(offsetof(FLGINIB,acvct));
const unsigned_t TOPPERS_cfg_sizeof_DTQINIB = (unsigned_t)(sizeof(DTQINIB));
const unsigned_t TOPPERS_cfg_offsetof_DTQINIB_dtqatr = (unsigned_t)(offsetof(DTQINIB,dtqatr));
const unsigned_t TOPPERS_cfg_offsetof_DTQINIB_dtqcnt = (unsigned_t)(offsetof(DTQINIB,dtqcnt));
const unsigned_t TOPPERS_cfg_offsetof_DTQINIB_p_dtqmb = (unsigned_t)(offsetof(DTQINIB,p_dtqmb));
const unsigned_t TOPPERS_cfg_offsetof_DTQINIB_acvct = (unsigned_t)(offsetof(DTQINIB,acvct));
const unsigned_t TOPPERS_cfg_sizeof_PDQINIB = (unsigned_t)(sizeof(PDQINIB));
const unsigned_t TOPPERS_cfg_offsetof_PDQINIB_pdqatr = (unsigned_t)(offsetof(PDQINIB,pdqatr));
const unsigned_t TOPPERS_cfg_offsetof_PDQINIB_pdqcnt = (unsigned_t)(offsetof(PDQINIB,pdqcnt));
const unsigned_t TOPPERS_cfg_offsetof_PDQINIB_maxdpri = (unsigned_t)(offsetof(PDQINIB,maxdpri));
const unsigned_t TOPPERS_cfg_offsetof_PDQINIB_p_pdqmb = (unsigned_t)(offsetof(PDQINIB,p_pdqmb));
const unsigned_t TOPPERS_cfg_offsetof_PDQINIB_acvct = (unsigned_t)(offsetof(PDQINIB,acvct));
const unsigned_t TOPPERS_cfg_sizeof_MTXINIB = (unsigned_t)(sizeof(MTXINIB));
const unsigned_t TOPPERS_cfg_offsetof_MTXINIB_mtxatr = (unsigned_t)(offsetof(MTXINIB,mtxatr));
const unsigned_t TOPPERS_cfg_offsetof_MTXINIB_ceilpri = (unsigned_t)(offsetof(MTXINIB,ceilpri));
const unsigned_t TOPPERS_cfg_offsetof_MTXINIB_acvct = (unsigned_t)(offsetof(MTXINIB,acvct));
const unsigned_t TOPPERS_cfg_sizeof_MBFINIB = (unsigned_t)(sizeof(MBFINIB));
const unsigned_t TOPPERS_cfg_offsetof_MBFINIB_mbfatr = (unsigned_t)(offsetof(MBFINIB,mbfatr));
const unsigned_t TOPPERS_cfg_offsetof_MBFINIB_maxmsz = (unsigned_t)(offsetof(MBFINIB,maxmsz));
const unsigned_t TOPPERS_cfg_offsetof_MBFINIB_mbfsz = (unsigned_t)(offsetof(MBFINIB,mbfsz));
const unsigned_t TOPPERS_cfg_offsetof_MBFINIB_mbfmb = (unsigned_t)(offsetof(MBFINIB,mbfmb));
const unsigned_t TOPPERS_cfg_sizeof_MPFINIB = (unsigned_t)(sizeof(MPFINIB));
const unsigned_t TOPPERS_cfg_offsetof_MPFINIB_mpfatr = (unsigned_t)(offsetof(MPFINIB,mpfatr));
const unsigned_t TOPPERS_cfg_offsetof_MPFINIB_blkcnt = (unsigned_t)(offsetof(MPFINIB,blkcnt));
const unsigned_t TOPPERS_cfg_offsetof_MPFINIB_blksz = (unsigned_t)(offsetof(MPFINIB,blksz));
const unsigned_t TOPPERS_cfg_offsetof_MPFINIB_mpf = (unsigned_t)(offsetof(MPFINIB,mpf));
const unsigned_t TOPPERS_cfg_offsetof_MPFINIB_p_mpfmb = (unsigned_t)(offsetof(MPFINIB,p_mpfmb));
const unsigned_t TOPPERS_cfg_offsetof_MPFINIB_acvct = (unsigned_t)(offsetof(MPFINIB,acvct));
const unsigned_t TOPPERS_cfg_sizeof_CYCINIB = (unsigned_t)(sizeof(CYCINIB));
const unsigned_t TOPPERS_cfg_offsetof_CYCINIB_cycatr = (unsigned_t)(offsetof(CYCINIB,cycatr));
const unsigned_t TOPPERS_cfg_offsetof_CYCINIB_exinf = (unsigned_t)(offsetof(CYCINIB,exinf));
const unsigned_t TOPPERS_cfg_offsetof_CYCINIB_nfyhdr = (unsigned_t)(offsetof(CYCINIB,nfyhdr));
const unsigned_t TOPPERS_cfg_offsetof_CYCINIB_cyctim = (unsigned_t)(offsetof(CYCINIB,cyctim));
const unsigned_t TOPPERS_cfg_offsetof_CYCINIB_cycphs = (unsigned_t)(offsetof(CYCINIB,cycphs));
const unsigned_t TOPPERS_cfg_offsetof_CYCINIB_acvct = (unsigned_t)(offsetof(CYCINIB,acvct));
const unsigned_t TOPPERS_cfg_sizeof_ALMINIB = (unsigned_t)(sizeof(ALMINIB));
const unsigned_t TOPPERS_cfg_offsetof_ALMINIB_almatr = (unsigned_t)(offsetof(ALMINIB,almatr));
const unsigned_t TOPPERS_cfg_offsetof_ALMINIB_exinf = (unsigned_t)(offsetof(ALMINIB,exinf));
const unsigned_t TOPPERS_cfg_offsetof_ALMINIB_nfyhdr = (unsigned_t)(offsetof(ALMINIB,nfyhdr));
const unsigned_t TOPPERS_cfg_offsetof_ALMINIB_acvct = (unsigned_t)(offsetof(ALMINIB,acvct));
#if !defined(OMIT_INITIALIZE_INTERRUPT)
const unsigned_t TOPPERS_cfg_sizeof_INHINIB = (unsigned_t)(sizeof(INHINIB));
#endif
#if !defined(OMIT_INITIALIZE_INTERRUPT)
const unsigned_t TOPPERS_cfg_offset_INHINIB_inhno = (unsigned_t)(offsetof(INHINIB,inhno));
#endif
#if !defined(OMIT_INITIALIZE_INTERRUPT)
const unsigned_t TOPPERS_cfg_offset_INHINIB_inhatr = (unsigned_t)(offsetof(INHINIB,inhatr));
#endif
#if !defined(OMIT_INITIALIZE_INTERRUPT)
const unsigned_t TOPPERS_cfg_offset_INHINIB_int_entry = (unsigned_t)(offsetof(INHINIB,int_entry));
#endif
#if !defined(OMIT_INITIALIZE_INTERRUPT)
const unsigned_t TOPPERS_cfg_sizeof_INTINIB = (unsigned_t)(sizeof(INTINIB));
#endif
#if !defined(OMIT_INITIALIZE_INTERRUPT)
const unsigned_t TOPPERS_cfg_offset_INTINIB_intno = (unsigned_t)(offsetof(INTINIB,intno));
#endif
#if !defined(OMIT_INITIALIZE_INTERRUPT)
const unsigned_t TOPPERS_cfg_offset_INTINIB_intatr = (unsigned_t)(offsetof(INTINIB,intatr));
#endif
#if !defined(OMIT_INITIALIZE_INTERRUPT)
const unsigned_t TOPPERS_cfg_offset_INTINIB_intpri = (unsigned_t)(offsetof(INTINIB,intpri));
#endif
#if !defined(OMIT_INITIALIZE_EXCEPTION)
const unsigned_t TOPPERS_cfg_sizeof_EXCINIB = (unsigned_t)(sizeof(EXCINIB));
#endif
#if !defined(OMIT_INITIALIZE_EXCEPTION)
const unsigned_t TOPPERS_cfg_offset_EXCINIB_excno = (unsigned_t)(offsetof(EXCINIB,excno));
#endif
#if !defined(OMIT_INITIALIZE_EXCEPTION)
const unsigned_t TOPPERS_cfg_offset_EXCINIB_excatr = (unsigned_t)(offsetof(EXCINIB,excatr));
#endif
#if !defined(OMIT_INITIALIZE_EXCEPTION)
const unsigned_t TOPPERS_cfg_offset_EXCINIB_exc_entry = (unsigned_t)(offsetof(EXCINIB,exc_entry));
#endif
const unsigned_t TOPPERS_cfg_sizeof_SVCINIB = (unsigned_t)(sizeof(SVCINIB));
const unsigned_t TOPPERS_cfg_offsetof_SVCINIB_svcrtn = (unsigned_t)(offsetof(SVCINIB,svcrtn));
const unsigned_t TOPPERS_cfg_offsetof_SVCINIB_stksz = (unsigned_t)(offsetof(SVCINIB,stksz));
const unsigned_t TOPPERS_cfg_sizeof_MEMINIB = (unsigned_t)(sizeof(MEMINIB));
#if !defined(OMIT_STANDARD_MEMINIB)
const unsigned_t TOPPERS_cfg_offsetof_MEMINIB_accatr = (unsigned_t)(offsetof(MEMINIB,accatr));
#endif
#if !defined(OMIT_STANDARD_MEMINIB)
const unsigned_t TOPPERS_cfg_offsetof_MEMINIB_acptn1 = (unsigned_t)(offsetof(MEMINIB,acptn1));
#endif
#if !defined(OMIT_STANDARD_MEMINIB)
const unsigned_t TOPPERS_cfg_offsetof_MEMINIB_acptn2 = (unsigned_t)(offsetof(MEMINIB,acptn2));
#endif
#if !defined(OMIT_STANDARD_MEMINIB)
const unsigned_t TOPPERS_cfg_offsetof_MEMINIB_acptn4 = (unsigned_t)(offsetof(MEMINIB,acptn4));
#endif
#if defined(TARGET_RZA1H)
const signed_t TOPPERS_cfg_TARGET_RZA1H = (signed_t)(true);
#endif
#if defined(TARGET_RZA1L)
const signed_t TOPPERS_cfg_TARGET_RZA1L = (signed_t)(true);
#endif
const unsigned_t TOPPERS_cfg_TA_NEGEDGE = (unsigned_t)(TA_NEGEDGE);
const unsigned_t TOPPERS_cfg_TA_POSEDGE = (unsigned_t)(TA_POSEDGE);
const unsigned_t TOPPERS_cfg_TA_BOTHEDGE = (unsigned_t)(TA_BOTHEDGE);
const unsigned_t TOPPERS_cfg_INTNO_IRQ0 = (unsigned_t)(INTNO_IRQ0);
const unsigned_t TOPPERS_cfg_INTNO_IRQ7 = (unsigned_t)(INTNO_IRQ7);
const unsigned_t TOPPERS_cfg_TARGET_ARCH_ARM = (unsigned_t)(__TARGET_ARCH_ARM);
#if defined(USE_ARM_MMU)
const signed_t TOPPERS_cfg_USE_ARM_MMU = (signed_t)(true);
#endif
#if defined(USE_ARM_SSECTION)
const signed_t TOPPERS_cfg_USE_ARM_SSECTION = (signed_t)(true);
#endif
#if defined(USE_INTCFG_TABLE)
const signed_t TOPPERS_cfg_USE_INTCFG_TABLE = (signed_t)(true);
#endif
const unsigned_t TOPPERS_cfg_TA_SORDER = (unsigned_t)(TA_SORDER);
const unsigned_t TOPPERS_cfg_TA_WTHROUGH = (unsigned_t)(TA_WTHROUGH);
const unsigned_t TOPPERS_cfg_TA_NONSHARED = (unsigned_t)(TA_NONSHARED);
const unsigned_t TOPPERS_cfg_ARM_SSECTION_SIZE = (unsigned_t)(ARM_SSECTION_SIZE);
const unsigned_t TOPPERS_cfg_ARM_SECTION_SIZE = (unsigned_t)(ARM_SECTION_SIZE);
const unsigned_t TOPPERS_cfg_ARM_LPAGE_SIZE = (unsigned_t)(ARM_LPAGE_SIZE);
const unsigned_t TOPPERS_cfg_ARM_PAGE_SIZE = (unsigned_t)(ARM_PAGE_SIZE);
const unsigned_t TOPPERS_cfg_ARM_SECTION_TABLE_SIZE = (unsigned_t)(ARM_SECTION_TABLE_SIZE);
const unsigned_t TOPPERS_cfg_ARM_SECTION_TABLE_ALIGN = (unsigned_t)(ARM_SECTION_TABLE_ALIGN);
const unsigned_t TOPPERS_cfg_ARM_SECTION_TABLE_ENTRY = (unsigned_t)(ARM_SECTION_TABLE_ENTRY);
const unsigned_t TOPPERS_cfg_ARM_PAGE_TABLE_SIZE = (unsigned_t)(ARM_PAGE_TABLE_SIZE);
const unsigned_t TOPPERS_cfg_ARM_PAGE_TABLE_ALIGN = (unsigned_t)(ARM_PAGE_TABLE_ALIGN);
const unsigned_t TOPPERS_cfg_ARM_PAGE_TABLE_ENTRY = (unsigned_t)(ARM_PAGE_TABLE_ENTRY);
#if defined(ARM_PAGE_TABLE_RATIO)
const unsigned_t TOPPERS_cfg_ARM_PAGE_TABLE_RATIO = (unsigned_t)(ARM_PAGE_TABLE_RATIO);
#endif
const unsigned_t TOPPERS_cfg_sizeof_TCB = (unsigned_t)(sizeof(TCB));
const unsigned_t TOPPERS_cfg_offsetof_TCB_p_tinib = (unsigned_t)(offsetof(TCB,p_tinib));
const unsigned_t TOPPERS_cfg_offsetof_TCB_p_dominib = (unsigned_t)(offsetof(TCB,p_dominib));
const unsigned_t TOPPERS_cfg_offsetof_TCB_svclevel = (unsigned_t)(offsetof(TCB,svclevel));
const unsigned_t TOPPERS_cfg_offsetof_TCB_sp = (unsigned_t)(offsetof(TCB,tskctxb.sp));
const unsigned_t TOPPERS_cfg_offsetof_TCB_pc = (unsigned_t)(offsetof(TCB,tskctxb.pc));
const unsigned_t TOPPERS_cfg_offsetof_DOMINIB_p_section_table = (unsigned_t)(offsetof(DOMINIB,domctxb.p_section_table));
const unsigned_t TOPPERS_cfg_offsetof_T_EXCINF_rundom = (unsigned_t)(offsetof(T_EXCINF,_kernel_rundom));
const unsigned_t TOPPERS_cfg_offsetof_T_EXCINF_cpsr = (unsigned_t)(offsetof(T_EXCINF,cpsr));

#define rDomain1 1
#define rDomain2 2

#ifdef TOPPERS_ML_AUTO

#ifdef TOPPERS_EXECUTE_ON_ROM

#line 18 "../target/gr_peach_gcc/target_mem.cfg"
const unsigned_t TOPPERS_cfg_static_api_1 = 1;
#line 18 "../target/gr_peach_gcc/target_mem.cfg"
const unsigned_t TOPPERS_cfg_valueof_regatr_1 = (unsigned_t)(TA_NOWRITE);
#line 18 "../target/gr_peach_gcc/target_mem.cfg"
const uintptr_t TOPPERS_cfg_valueof_base_1 = (uintptr_t)(0x18000000);
#line 18 "../target/gr_peach_gcc/target_mem.cfg"
const unsigned_t TOPPERS_cfg_valueof_size_1 = (unsigned_t)(0x00004000);

#line 19 "../target/gr_peach_gcc/target_mem.cfg"
const unsigned_t TOPPERS_cfg_static_api_2 = 2;
#line 19 "../target/gr_peach_gcc/target_mem.cfg"
const unsigned_t TOPPERS_cfg_valueof_mematr_2 = (unsigned_t)(TA_NOWRITE|TA_EXEC);

#line 21 "../target/gr_peach_gcc/target_mem.cfg"
const unsigned_t TOPPERS_cfg_static_api_3 = 3;
#line 21 "../target/gr_peach_gcc/target_mem.cfg"
const unsigned_t TOPPERS_cfg_valueof_regatr_3 = (unsigned_t)(TA_NOWRITE);
#line 21 "../target/gr_peach_gcc/target_mem.cfg"
const uintptr_t TOPPERS_cfg_valueof_base_3 = (uintptr_t)(0x18004000);
#line 21 "../target/gr_peach_gcc/target_mem.cfg"
const unsigned_t TOPPERS_cfg_valueof_size_3 = (unsigned_t)(0x07FFC000);

#line 22 "../target/gr_peach_gcc/target_mem.cfg"
const unsigned_t TOPPERS_cfg_static_api_4 = 4;
#line 22 "../target/gr_peach_gcc/target_mem.cfg"
const unsigned_t TOPPERS_cfg_valueof_regatr_4 = (unsigned_t)(TA_NULL);
#line 22 "../target/gr_peach_gcc/target_mem.cfg"
const uintptr_t TOPPERS_cfg_valueof_base_4 = (uintptr_t)(0x20000000);
#line 22 "../target/gr_peach_gcc/target_mem.cfg"
const unsigned_t TOPPERS_cfg_valueof_size_4 = (unsigned_t)(0x00a00000);

#else

#line 31 "../target/gr_peach_gcc/target_mem.cfg"
const unsigned_t TOPPERS_cfg_static_api_5 = 5;
#line 31 "../target/gr_peach_gcc/target_mem.cfg"
const unsigned_t TOPPERS_cfg_valueof_regatr_5 = (unsigned_t)(TA_NOWRITE);
#line 31 "../target/gr_peach_gcc/target_mem.cfg"
const uintptr_t TOPPERS_cfg_valueof_base_5 = (uintptr_t)(0x20000000);
#line 31 "../target/gr_peach_gcc/target_mem.cfg"
const unsigned_t TOPPERS_cfg_valueof_size_5 = (unsigned_t)(0x00100000);

#line 32 "../target/gr_peach_gcc/target_mem.cfg"
const unsigned_t TOPPERS_cfg_static_api_6 = 6;
#line 32 "../target/gr_peach_gcc/target_mem.cfg"
const unsigned_t TOPPERS_cfg_valueof_regatr_6 = (unsigned_t)(TA_NULL);
#line 32 "../target/gr_peach_gcc/target_mem.cfg"
const uintptr_t TOPPERS_cfg_valueof_base_6 = (uintptr_t)(0x20100000);
#line 32 "../target/gr_peach_gcc/target_mem.cfg"
const unsigned_t TOPPERS_cfg_valueof_size_6 = (unsigned_t)(0x00900000);

#endif

#line 39 "../target/gr_peach_gcc/target_mem.cfg"
const unsigned_t TOPPERS_cfg_static_api_7 = 7;

#line 45 "../target/gr_peach_gcc/target_mem.cfg"
const unsigned_t TOPPERS_cfg_static_api_8 = 8;
#line 45 "../target/gr_peach_gcc/target_mem.cfg"
const unsigned_t TOPPERS_cfg_valueof_mematr_8 = (unsigned_t)(TA_NOWRITE|TA_EXEC);

#endif

#line 54 "../target/gr_peach_gcc/target_mem.cfg"
const unsigned_t TOPPERS_cfg_static_api_9 = 9;
#line 54 "../target/gr_peach_gcc/target_mem.cfg"
const unsigned_t TOPPERS_cfg_valueof_accatr_9 = (unsigned_t)(TA_IODEV);
#line 54 "../target/gr_peach_gcc/target_mem.cfg"
const unsigned_t TOPPERS_cfg_valueof_size_9 = (unsigned_t)(IO1_SIZE);

#line 55 "../target/gr_peach_gcc/target_mem.cfg"
const unsigned_t TOPPERS_cfg_static_api_10 = 10;
#line 55 "../target/gr_peach_gcc/target_mem.cfg"
const unsigned_t TOPPERS_cfg_valueof_accatr_10 = (unsigned_t)(TA_IODEV);
#line 55 "../target/gr_peach_gcc/target_mem.cfg"
const unsigned_t TOPPERS_cfg_valueof_size_10 = (unsigned_t)(IO2_SIZE);

#line 11 "../arch/arm_gcc/rza1/chip_timer.cfg"
const unsigned_t TOPPERS_cfg_static_api_11 = 11;
#line 11 "../arch/arm_gcc/rza1/chip_timer.cfg"
const unsigned_t TOPPERS_cfg_valueof_iniatr_11 = (unsigned_t)(TA_NULL);

#line 12 "../arch/arm_gcc/rza1/chip_timer.cfg"
const unsigned_t TOPPERS_cfg_static_api_12 = 12;
#line 12 "../arch/arm_gcc/rza1/chip_timer.cfg"
const unsigned_t TOPPERS_cfg_valueof_teratr_12 = (unsigned_t)(TA_NULL);

#line 14 "../arch/arm_gcc/rza1/chip_timer.cfg"
const unsigned_t TOPPERS_cfg_static_api_13 = 13;
#line 14 "../arch/arm_gcc/rza1/chip_timer.cfg"
const unsigned_t TOPPERS_cfg_valueof_intno_13 = (unsigned_t)(INTNO_TIMER);
#line 14 "../arch/arm_gcc/rza1/chip_timer.cfg"
const unsigned_t TOPPERS_cfg_valueof_intatr_13 = (unsigned_t)(TA_ENAINT|INTATR_TIMER);
#line 14 "../arch/arm_gcc/rza1/chip_timer.cfg"
const signed_t TOPPERS_cfg_valueof_intpri_13 = (signed_t)(INTPRI_TIMER);

#line 15 "../arch/arm_gcc/rza1/chip_timer.cfg"
const unsigned_t TOPPERS_cfg_static_api_14 = 14;
#line 15 "../arch/arm_gcc/rza1/chip_timer.cfg"
const unsigned_t TOPPERS_cfg_valueof_inhno_14 = (unsigned_t)(INHNO_TIMER);
#line 15 "../arch/arm_gcc/rza1/chip_timer.cfg"
const unsigned_t TOPPERS_cfg_valueof_inhatr_14 = (unsigned_t)(TA_NULL);

#line 22 "../arch/arm_gcc/rza1/chip_timer.cfg"
const unsigned_t TOPPERS_cfg_static_api_15 = 15;
#line 22 "../arch/arm_gcc/rza1/chip_timer.cfg"
const unsigned_t TOPPERS_cfg_valueof_iniatr_15 = (unsigned_t)(TA_NULL);

#line 23 "../arch/arm_gcc/rza1/chip_timer.cfg"
const unsigned_t TOPPERS_cfg_static_api_16 = 16;
#line 23 "../arch/arm_gcc/rza1/chip_timer.cfg"
const unsigned_t TOPPERS_cfg_valueof_teratr_16 = (unsigned_t)(TA_NULL);

#line 25 "../arch/arm_gcc/rza1/chip_timer.cfg"
const unsigned_t TOPPERS_cfg_static_api_17 = 17;
#line 25 "../arch/arm_gcc/rza1/chip_timer.cfg"
const unsigned_t TOPPERS_cfg_valueof_intno_17 = (unsigned_t)(INTNO_TWDTIMER);
#line 25 "../arch/arm_gcc/rza1/chip_timer.cfg"
const unsigned_t TOPPERS_cfg_valueof_intatr_17 = (unsigned_t)(TA_ENAINT|INTATR_TWDTIMER);
#line 25 "../arch/arm_gcc/rza1/chip_timer.cfg"
const signed_t TOPPERS_cfg_valueof_intpri_17 = (signed_t)(INTPRI_TWDTIMER);

#line 26 "../arch/arm_gcc/rza1/chip_timer.cfg"
const unsigned_t TOPPERS_cfg_static_api_18 = 18;
#line 26 "../arch/arm_gcc/rza1/chip_timer.cfg"
const unsigned_t TOPPERS_cfg_valueof_inhno_18 = (unsigned_t)(INHNO_TWDTIMER);
#line 26 "../arch/arm_gcc/rza1/chip_timer.cfg"
const unsigned_t TOPPERS_cfg_valueof_inhatr_18 = (unsigned_t)(TA_NULL);

#line 1 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_static_api_19 = 19;

#line 2 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_static_api_20 = 20;

#line 3 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_static_api_21 = 21;

#line 4 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_static_api_22 = 22;

#line 5 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_static_api_23 = 23;

#line 6 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_static_api_24 = 24;

#line 7 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_static_api_25 = 25;

#line 8 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_static_api_26 = 26;

#line 9 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_static_api_27 = 27;

#line 10 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_static_api_28 = 28;

#line 11 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_static_api_29 = 29;

#line 12 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_static_api_30 = 30;

#line 13 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_static_api_31 = 31;

#line 14 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_static_api_32 = 32;

#line 15 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_static_api_33 = 33;

#line 16 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_static_api_34 = 34;

#line 17 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_static_api_35 = 35;

#line 18 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_static_api_36 = 36;

#line 19 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_static_api_37 = 37;

#line 20 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_static_api_38 = 38;

#line 21 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_static_api_39 = 39;

#line 22 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_static_api_40 = 40;

#line 23 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_static_api_41 = 41;

#line 24 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_static_api_42 = 42;

#line 25 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_static_api_43 = 43;

#line 26 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_static_api_44 = 44;

#line 27 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_static_api_45 = 45;

#line 28 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_static_api_46 = 46;

#line 29 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_static_api_47 = 47;

#line 30 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_static_api_48 = 48;

#line 31 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_static_api_49 = 49;

#line 32 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_static_api_50 = 50;

#line 33 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_static_api_51 = 51;
#define TSKID_tTask_rKernelDomain_MainTask	(<>)
#line 33 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_valueof_tskatr_51 = (unsigned_t)(TA_ACT);
#line 33 "./gen/tecsgen_rKernelDomain.cfg"
const signed_t TOPPERS_cfg_valueof_itskpri_51 = (signed_t)(MAIN_PRIORITY);
#line 33 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_valueof_stksz_51 = (unsigned_t)(STACK_SIZE);

#line 34 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_static_api_52 = 52;
#line 34 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_valueof_acptn1_52 = (unsigned_t)(TACP_KERNEL);
#line 34 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_valueof_acptn2_52 = (unsigned_t)(TACP_KERNEL);
#line 34 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_valueof_acptn3_52 = (unsigned_t)(TACP_KERNEL);
#line 34 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_valueof_acptn4_52 = (unsigned_t)(TACP_KERNEL);

#line 35 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_static_api_53 = 53;
#define TSKID_tTask_rKernelDomain_ExceptionTask	(<>)
#line 35 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_valueof_tskatr_53 = (unsigned_t)(TA_NULL);
#line 35 "./gen/tecsgen_rKernelDomain.cfg"
const signed_t TOPPERS_cfg_valueof_itskpri_53 = (signed_t)(EXC_PRIORITY);
#line 35 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_valueof_stksz_53 = (unsigned_t)(STACK_SIZE);

#line 36 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_static_api_54 = 54;
#line 36 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_valueof_acptn1_54 = (unsigned_t)(TACP_KERNEL);
#line 36 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_valueof_acptn2_54 = (unsigned_t)(TACP_KERNEL);
#line 36 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_valueof_acptn3_54 = (unsigned_t)(TACP_KERNEL);
#line 36 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_valueof_acptn4_54 = (unsigned_t)(TACP_KERNEL);

#line 37 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_static_api_55 = 55;
#define TSKID_tTask_rKernelDomain_LogTask_Task	(<>)
#line 37 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_valueof_tskatr_55 = (unsigned_t)(TA_ACT);
#line 37 "./gen/tecsgen_rKernelDomain.cfg"
const signed_t TOPPERS_cfg_valueof_itskpri_55 = (signed_t)(3);
#line 37 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_valueof_stksz_55 = (unsigned_t)(LogTaskStackSize);

#line 38 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_static_api_56 = 56;
#line 38 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_valueof_acptn1_56 = (unsigned_t)(TACP_KERNEL);
#line 38 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_valueof_acptn2_56 = (unsigned_t)(TACP_KERNEL);
#line 38 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_valueof_acptn3_56 = (unsigned_t)(TACP_KERNEL);
#line 38 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_valueof_acptn4_56 = (unsigned_t)(TACP_KERNEL);

#line 39 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_static_api_57 = 57;
#define SEMID_tSemaphore_rKernelDomain_SerialPort1_ReceiveSemaphore	(<>)
#line 39 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_valueof_sematr_57 = (unsigned_t)(TA_NULL);
#line 39 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_valueof_isemcnt_57 = (unsigned_t)(0);
#line 39 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_valueof_maxsem_57 = (unsigned_t)(1);

#line 40 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_static_api_58 = 58;
#line 40 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_valueof_acptn1_58 = (unsigned_t)(TACP_KERNEL);
#line 40 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_valueof_acptn2_58 = (unsigned_t)(TACP_KERNEL);
#line 40 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_valueof_acptn3_58 = (unsigned_t)(TACP_KERNEL);
#line 40 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_valueof_acptn4_58 = (unsigned_t)(TACP_KERNEL);

#line 41 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_static_api_59 = 59;
#define SEMID_tSemaphore_rKernelDomain_SerialPort1_SendSemaphore	(<>)
#line 41 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_valueof_sematr_59 = (unsigned_t)(TA_NULL);
#line 41 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_valueof_isemcnt_59 = (unsigned_t)(1);
#line 41 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_valueof_maxsem_59 = (unsigned_t)(1);

#line 42 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_static_api_60 = 60;
#line 42 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_valueof_acptn1_60 = (unsigned_t)(TACP_KERNEL);
#line 42 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_valueof_acptn2_60 = (unsigned_t)(TACP_KERNEL);
#line 42 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_valueof_acptn3_60 = (unsigned_t)(TACP_KERNEL);
#line 42 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_valueof_acptn4_60 = (unsigned_t)(TACP_KERNEL);

#line 43 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_static_api_61 = 61;
#line 43 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_valueof_intno_61 = (unsigned_t)(INTNO1);
#line 43 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_valueof_intatr_61 = (unsigned_t)(INTNO1_INTATR);
#line 43 "./gen/tecsgen_rKernelDomain.cfg"
const signed_t TOPPERS_cfg_valueof_intpri_61 = (signed_t)(INTNO1_INTPRI);

#line 44 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_static_api_62 = 62;
#line 44 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_valueof_intno_62 = (unsigned_t)(INTNO_SCIF2_RXI);
#line 44 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_valueof_intatr_62 = (unsigned_t)(TA_NULL);
#line 44 "./gen/tecsgen_rKernelDomain.cfg"
const signed_t TOPPERS_cfg_valueof_intpri_62 = (signed_t)(-4);

#line 45 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_static_api_63 = 63;
#line 45 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_valueof_intno_63 = (unsigned_t)(INTNO_SCIF2_TXI);
#line 45 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_valueof_intatr_63 = (unsigned_t)(TA_NULL);
#line 45 "./gen/tecsgen_rKernelDomain.cfg"
const signed_t TOPPERS_cfg_valueof_intpri_63 = (signed_t)(-4);

#line 46 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_static_api_64 = 64;
#define ISRID_tISR_rKernelDomain_InterruptServiceRoutine	(<>)
#line 46 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_valueof_isratr_64 = (unsigned_t)(TA_NULL);
#line 46 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_valueof_intno_64 = (unsigned_t)(INTNO1);
#line 46 "./gen/tecsgen_rKernelDomain.cfg"
const signed_t TOPPERS_cfg_valueof_isrpri_64 = (signed_t)(1);

#line 47 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_static_api_65 = 65;
#define ISRID_tISR_rKernelDomain_SIOPortTarget1_RxISRInstance	(<>)
#line 47 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_valueof_isratr_65 = (unsigned_t)(TA_NULL);
#line 47 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_valueof_intno_65 = (unsigned_t)(INTNO_SCIF2_RXI);
#line 47 "./gen/tecsgen_rKernelDomain.cfg"
const signed_t TOPPERS_cfg_valueof_isrpri_65 = (signed_t)(1);

#line 48 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_static_api_66 = 66;
#define ISRID_tISR_rKernelDomain_SIOPortTarget1_TxISRInstance	(<>)
#line 48 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_valueof_isratr_66 = (unsigned_t)(TA_NULL);
#line 48 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_valueof_intno_66 = (unsigned_t)(INTNO_SCIF2_TXI);
#line 48 "./gen/tecsgen_rKernelDomain.cfg"
const signed_t TOPPERS_cfg_valueof_isrpri_66 = (signed_t)(1);

#line 49 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_static_api_67 = 67;
#line 49 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_valueof_excno_67 = (unsigned_t)(CPUEXC1);
#line 49 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_valueof_excatr_67 = (unsigned_t)(TA_NULL);

#line 50 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_static_api_68 = 68;
#line 50 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_valueof_iniatr_68 = (unsigned_t)(TA_NULL);

#line 51 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_static_api_69 = 69;
#line 51 "./gen/tecsgen_rKernelDomain.cfg"
const unsigned_t TOPPERS_cfg_valueof_teratr_69 = (unsigned_t)(TA_NULL);

#line 1 "./gen/tecsgen_rDomain1.cfg"
const unsigned_t TOPPERS_cfg_static_api_70 = 70;

#line 2 "./gen/tecsgen_rDomain1.cfg"
const unsigned_t TOPPERS_cfg_static_api_71 = 71;

#line 3 "./gen/tecsgen_rDomain1.cfg"
const unsigned_t TOPPERS_cfg_static_api_72 = 72;

#line 4 "./gen/tecsgen_rDomain1.cfg"
const unsigned_t TOPPERS_cfg_static_api_73 = 73;
#define TSKID_tTask_rDomain1_Task1	(<>)
#line 4 "./gen/tecsgen_rDomain1.cfg"
const unsigned_t TOPPERS_cfg_valueof_tskatr_73 = (unsigned_t)(TA_NULL);
#line 4 "./gen/tecsgen_rDomain1.cfg"
const signed_t TOPPERS_cfg_valueof_itskpri_73 = (signed_t)(MID_PRIORITY);
#line 4 "./gen/tecsgen_rDomain1.cfg"
const unsigned_t TOPPERS_cfg_valueof_stksz_73 = (unsigned_t)(STACK_SIZE);

#line 5 "./gen/tecsgen_rDomain1.cfg"
const unsigned_t TOPPERS_cfg_static_api_74 = 74;
#line 5 "./gen/tecsgen_rDomain1.cfg"
const unsigned_t TOPPERS_cfg_valueof_acptn1_74 = (unsigned_t)(TACP(rDomain1));
#line 5 "./gen/tecsgen_rDomain1.cfg"
const unsigned_t TOPPERS_cfg_valueof_acptn2_74 = (unsigned_t)(TACP(rDomain1));
#line 5 "./gen/tecsgen_rDomain1.cfg"
const unsigned_t TOPPERS_cfg_valueof_acptn3_74 = (unsigned_t)(TACP(rDomain1));
#line 5 "./gen/tecsgen_rDomain1.cfg"
const unsigned_t TOPPERS_cfg_valueof_acptn4_74 = (unsigned_t)(TACP(rDomain1));

#line 6 "./gen/tecsgen_rDomain1.cfg"
const unsigned_t TOPPERS_cfg_static_api_75 = 75;
#define TSKID_tTask_rDomain1_Task2	(<>)
#line 6 "./gen/tecsgen_rDomain1.cfg"
const unsigned_t TOPPERS_cfg_valueof_tskatr_75 = (unsigned_t)(TA_NULL);
#line 6 "./gen/tecsgen_rDomain1.cfg"
const signed_t TOPPERS_cfg_valueof_itskpri_75 = (signed_t)(MID_PRIORITY);
#line 6 "./gen/tecsgen_rDomain1.cfg"
const unsigned_t TOPPERS_cfg_valueof_stksz_75 = (unsigned_t)(STACK_SIZE);

#line 7 "./gen/tecsgen_rDomain1.cfg"
const unsigned_t TOPPERS_cfg_static_api_76 = 76;
#line 7 "./gen/tecsgen_rDomain1.cfg"
const unsigned_t TOPPERS_cfg_valueof_acptn1_76 = (unsigned_t)(TACP(rDomain1));
#line 7 "./gen/tecsgen_rDomain1.cfg"
const unsigned_t TOPPERS_cfg_valueof_acptn2_76 = (unsigned_t)(TACP(rDomain1));
#line 7 "./gen/tecsgen_rDomain1.cfg"
const unsigned_t TOPPERS_cfg_valueof_acptn3_76 = (unsigned_t)(TACP(rDomain1));
#line 7 "./gen/tecsgen_rDomain1.cfg"
const unsigned_t TOPPERS_cfg_valueof_acptn4_76 = (unsigned_t)(TACP(rDomain1));

#line 8 "./gen/tecsgen_rDomain1.cfg"
const unsigned_t TOPPERS_cfg_static_api_77 = 77;
#define TSKID_tTask_rDomain1_AlarmTask	(<>)
#line 8 "./gen/tecsgen_rDomain1.cfg"
const unsigned_t TOPPERS_cfg_valueof_tskatr_77 = (unsigned_t)(TA_NULL);
#line 8 "./gen/tecsgen_rDomain1.cfg"
const signed_t TOPPERS_cfg_valueof_itskpri_77 = (signed_t)(ALM_PRIORITY);
#line 8 "./gen/tecsgen_rDomain1.cfg"
const unsigned_t TOPPERS_cfg_valueof_stksz_77 = (unsigned_t)(STACK_SIZE);

#line 9 "./gen/tecsgen_rDomain1.cfg"
const unsigned_t TOPPERS_cfg_static_api_78 = 78;
#line 9 "./gen/tecsgen_rDomain1.cfg"
const unsigned_t TOPPERS_cfg_valueof_acptn1_78 = (unsigned_t)(TACP(rDomain1));
#line 9 "./gen/tecsgen_rDomain1.cfg"
const unsigned_t TOPPERS_cfg_valueof_acptn2_78 = (unsigned_t)(TACP(rDomain1));
#line 9 "./gen/tecsgen_rDomain1.cfg"
const unsigned_t TOPPERS_cfg_valueof_acptn3_78 = (unsigned_t)(TACP(rDomain1));
#line 9 "./gen/tecsgen_rDomain1.cfg"
const unsigned_t TOPPERS_cfg_valueof_acptn4_78 = (unsigned_t)(TACP(rDomain1));

#line 1 "./gen/tecsgen_rDomain2.cfg"
const unsigned_t TOPPERS_cfg_static_api_79 = 79;

#line 2 "./gen/tecsgen_rDomain2.cfg"
const unsigned_t TOPPERS_cfg_static_api_80 = 80;
#define TSKID_tTask_rDomain2_Task3	(<>)
#line 2 "./gen/tecsgen_rDomain2.cfg"
const unsigned_t TOPPERS_cfg_valueof_tskatr_80 = (unsigned_t)(TA_NULL);
#line 2 "./gen/tecsgen_rDomain2.cfg"
const signed_t TOPPERS_cfg_valueof_itskpri_80 = (signed_t)(MID_PRIORITY);
#line 2 "./gen/tecsgen_rDomain2.cfg"
const unsigned_t TOPPERS_cfg_valueof_stksz_80 = (unsigned_t)(STACK_SIZE);

#line 3 "./gen/tecsgen_rDomain2.cfg"
const unsigned_t TOPPERS_cfg_static_api_81 = 81;
#line 3 "./gen/tecsgen_rDomain2.cfg"
const unsigned_t TOPPERS_cfg_valueof_acptn1_81 = (unsigned_t)(TACP(rDomain2));
#line 3 "./gen/tecsgen_rDomain2.cfg"
const unsigned_t TOPPERS_cfg_valueof_acptn2_81 = (unsigned_t)(TACP(rDomain2));
#line 3 "./gen/tecsgen_rDomain2.cfg"
const unsigned_t TOPPERS_cfg_valueof_acptn3_81 = (unsigned_t)(TACP(rDomain2));
#line 3 "./gen/tecsgen_rDomain2.cfg"
const unsigned_t TOPPERS_cfg_valueof_acptn4_81 = (unsigned_t)(TACP(rDomain2));

#line 25 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_static_api_82 = 82;

#line 26 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_static_api_83 = 83;

#line 27 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_static_api_84 = 84;

#line 28 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_static_api_85 = 85;

#line 29 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_static_api_86 = 86;

#line 30 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_static_api_87 = 87;

#line 31 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_static_api_88 = 88;

#line 32 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_static_api_89 = 89;

#line 33 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_static_api_90 = 90;

#line 34 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_static_api_91 = 91;

#line 35 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_static_api_92 = 92;

#line 50 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_static_api_93 = 93;
#line 50 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_valueof_fncd_93 = (unsigned_t)(TFN_TECSGEN_ORIGIN + 0);
#line 50 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_valueof_svcatr_93 = (unsigned_t)(TA_NULL);
#line 50 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_valueof_stksz_93 = (unsigned_t)(SSZ_tHRPSVCPlugin_sSysLogSVCCaller_SysLog_eSysLog_eThroughEntry_write);

#line 57 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_static_api_94 = 94;
#line 57 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_valueof_fncd_94 = (unsigned_t)(TFN_TECSGEN_ORIGIN + 1);
#line 57 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_valueof_svcatr_94 = (unsigned_t)(TA_NULL);
#line 57 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_valueof_stksz_94 = (unsigned_t)(SSZ_tHRPSVCPlugin_sSysLogSVCCaller_SysLog_eSysLog_eThroughEntry_read);

#line 64 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_static_api_95 = 95;
#line 64 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_valueof_fncd_95 = (unsigned_t)(TFN_TECSGEN_ORIGIN + 2);
#line 64 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_valueof_svcatr_95 = (unsigned_t)(TA_NULL);
#line 64 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_valueof_stksz_95 = (unsigned_t)(SSZ_tHRPSVCPlugin_sSysLogSVCCaller_SysLog_eSysLog_eThroughEntry_mask);

#line 71 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_static_api_96 = 96;
#line 71 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_valueof_fncd_96 = (unsigned_t)(TFN_TECSGEN_ORIGIN + 3);
#line 71 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_valueof_svcatr_96 = (unsigned_t)(TA_NULL);
#line 71 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_valueof_stksz_96 = (unsigned_t)(SSZ_tHRPSVCPlugin_sSysLogSVCCaller_SysLog_eSysLog_eThroughEntry_refer);

#line 78 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_static_api_97 = 97;
#line 78 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_valueof_fncd_97 = (unsigned_t)(TFN_TECSGEN_ORIGIN + 4);
#line 78 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_valueof_svcatr_97 = (unsigned_t)(TA_NULL);
#line 78 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_valueof_stksz_97 = (unsigned_t)(SSZ_tHRPSVCPlugin_sSysLogSVCCaller_SysLog_eSysLog_eThroughEntry_flush);

#line 85 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_static_api_98 = 98;
#line 85 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_valueof_fncd_98 = (unsigned_t)(TFN_TECSGEN_ORIGIN + 5);
#line 85 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_valueof_svcatr_98 = (unsigned_t)(TA_NULL);
#line 85 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_valueof_stksz_98 = (unsigned_t)(SSZ_tHRPSVCPlugin_sSerialPortSVCCaller_SerialPort1_eSerialPort_eThroughEntry_open);

#line 92 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_static_api_99 = 99;
#line 92 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_valueof_fncd_99 = (unsigned_t)(TFN_TECSGEN_ORIGIN + 6);
#line 92 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_valueof_svcatr_99 = (unsigned_t)(TA_NULL);
#line 92 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_valueof_stksz_99 = (unsigned_t)(SSZ_tHRPSVCPlugin_sSerialPortSVCCaller_SerialPort1_eSerialPort_eThroughEntry_close);

#line 99 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_static_api_100 = 100;
#line 99 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_valueof_fncd_100 = (unsigned_t)(TFN_TECSGEN_ORIGIN + 7);
#line 99 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_valueof_svcatr_100 = (unsigned_t)(TA_NULL);
#line 99 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_valueof_stksz_100 = (unsigned_t)(SSZ_tHRPSVCPlugin_sSerialPortSVCCaller_SerialPort1_eSerialPort_eThroughEntry_read);

#line 106 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_static_api_101 = 101;
#line 106 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_valueof_fncd_101 = (unsigned_t)(TFN_TECSGEN_ORIGIN + 8);
#line 106 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_valueof_svcatr_101 = (unsigned_t)(TA_NULL);
#line 106 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_valueof_stksz_101 = (unsigned_t)(SSZ_tHRPSVCPlugin_sSerialPortSVCCaller_SerialPort1_eSerialPort_eThroughEntry_write);

#line 113 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_static_api_102 = 102;
#line 113 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_valueof_fncd_102 = (unsigned_t)(TFN_TECSGEN_ORIGIN + 9);
#line 113 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_valueof_svcatr_102 = (unsigned_t)(TA_NULL);
#line 113 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_valueof_stksz_102 = (unsigned_t)(SSZ_tHRPSVCPlugin_sSerialPortSVCCaller_SerialPort1_eSerialPort_eThroughEntry_control);

#line 120 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_static_api_103 = 103;
#line 120 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_valueof_fncd_103 = (unsigned_t)(TFN_TECSGEN_ORIGIN + 10);
#line 120 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_valueof_svcatr_103 = (unsigned_t)(TA_NULL);
#line 120 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_valueof_stksz_103 = (unsigned_t)(SSZ_tHRPSVCPlugin_sSerialPortSVCCaller_SerialPort1_eSerialPort_eThroughEntry_refer);

#line 132 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_static_api_104 = 104;
#define CYCID_tCyclicHandler_CyclicHandler	(<>)
#line 132 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_valueof_cycatr_104 = (unsigned_t)(TA_NULL);
#line 132 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_valueof_nfymode_104 = (unsigned_t)(TNFY_HANDLER);
#line 132 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_valueof_cyctim_104 = (unsigned_t)(2000000);
#line 132 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_valueof_cycphs_104 = (unsigned_t)(0);

#line 133 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_static_api_105 = 105;
#line 133 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_valueof_acptn1_105 = (unsigned_t)(TACP_KERNEL);
#line 133 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_valueof_acptn2_105 = (unsigned_t)(TACP_KERNEL);
#line 133 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_valueof_acptn3_105 = (unsigned_t)(TACP_KERNEL);
#line 133 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_valueof_acptn4_105 = (unsigned_t)(TACP_KERNEL);

#line 139 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_static_api_106 = 106;
#define ALMID_tAlarmNotifier_rDomain1_AlarmNotifier	(<>)
#line 139 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_valueof_almatr_106 = (unsigned_t)(TA_NULL);
#line 139 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_valueof_nfymode_106 = (unsigned_t)(TNFY_ACTTSK);

#line 140 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_static_api_107 = 107;
#line 140 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_valueof_acptn1_107 = (unsigned_t)(TACP(rDomain1));
#line 140 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_valueof_acptn2_107 = (unsigned_t)(TACP(rDomain1));
#line 140 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_valueof_acptn3_107 = (unsigned_t)(TACP(rDomain1));
#line 140 "./gen/tecsgen.cfg"
const unsigned_t TOPPERS_cfg_valueof_acptn4_107 = (unsigned_t)(TACP(rDomain1));

#line 10 "../sample/tSample2.cfg"
const unsigned_t TOPPERS_cfg_static_api_108 = 108;
#line 10 "../sample/tSample2.cfg"
const unsigned_t TOPPERS_cfg_valueof_scyctim_108 = (unsigned_t)(15000);

#line 11 "../sample/tSample2.cfg"
const unsigned_t TOPPERS_cfg_static_api_109 = 109;
#define SOM1	(<>)
#line 11 "../sample/tSample2.cfg"
const unsigned_t TOPPERS_cfg_valueof_somatr_109 = (unsigned_t)(TA_NULL);

#line 12 "../sample/tSample2.cfg"
const unsigned_t TOPPERS_cfg_static_api_110 = 110;
#line 12 "../sample/tSample2.cfg"
const unsigned_t TOPPERS_cfg_valueof_domid_110 = (unsigned_t)(rDomain1);
#line 12 "../sample/tSample2.cfg"
const unsigned_t TOPPERS_cfg_valueof_twdord_110 = (unsigned_t)(1);
#line 12 "../sample/tSample2.cfg"
const unsigned_t TOPPERS_cfg_valueof_twdlen_110 = (unsigned_t)(4000);

#line 13 "../sample/tSample2.cfg"
const unsigned_t TOPPERS_cfg_static_api_111 = 111;
#line 13 "../sample/tSample2.cfg"
const unsigned_t TOPPERS_cfg_valueof_domid_111 = (unsigned_t)(rDomain2);
#line 13 "../sample/tSample2.cfg"
const unsigned_t TOPPERS_cfg_valueof_twdord_111 = (unsigned_t)(2);
#line 13 "../sample/tSample2.cfg"
const unsigned_t TOPPERS_cfg_valueof_twdlen_111 = (unsigned_t)(4000);

#line 14 "../sample/tSample2.cfg"
const unsigned_t TOPPERS_cfg_static_api_112 = 112;
#define SOM2	(<>)
#line 14 "../sample/tSample2.cfg"
const unsigned_t TOPPERS_cfg_valueof_somatr_112 = (unsigned_t)(TA_NULL);

#line 15 "../sample/tSample2.cfg"
const unsigned_t TOPPERS_cfg_static_api_113 = 113;
#line 15 "../sample/tSample2.cfg"
const unsigned_t TOPPERS_cfg_valueof_domid_113 = (unsigned_t)(rDomain1);
#line 15 "../sample/tSample2.cfg"
const unsigned_t TOPPERS_cfg_valueof_twdord_113 = (unsigned_t)(1);
#line 15 "../sample/tSample2.cfg"
const unsigned_t TOPPERS_cfg_valueof_twdlen_113 = (unsigned_t)(6000);

#line 16 "../sample/tSample2.cfg"
const unsigned_t TOPPERS_cfg_static_api_114 = 114;
#line 16 "../sample/tSample2.cfg"
const unsigned_t TOPPERS_cfg_valueof_domid_114 = (unsigned_t)(rDomain2);
#line 16 "../sample/tSample2.cfg"
const unsigned_t TOPPERS_cfg_valueof_twdord_114 = (unsigned_t)(2);
#line 16 "../sample/tSample2.cfg"
const unsigned_t TOPPERS_cfg_valueof_twdlen_114 = (unsigned_t)(2000);

#line 17 "../sample/tSample2.cfg"
const unsigned_t TOPPERS_cfg_static_api_115 = 115;
#define SOM3	(<>)
#line 17 "../sample/tSample2.cfg"
const unsigned_t TOPPERS_cfg_valueof_somatr_115 = (unsigned_t)(TA_NULL);

#line 18 "../sample/tSample2.cfg"
const unsigned_t TOPPERS_cfg_static_api_116 = 116;
#line 18 "../sample/tSample2.cfg"
const unsigned_t TOPPERS_cfg_valueof_domid_116 = (unsigned_t)(rDomain1);
#line 18 "../sample/tSample2.cfg"
const unsigned_t TOPPERS_cfg_valueof_twdord_116 = (unsigned_t)(1);
#line 18 "../sample/tSample2.cfg"
const unsigned_t TOPPERS_cfg_valueof_twdlen_116 = (unsigned_t)(5000);

#line 19 "../sample/tSample2.cfg"
const unsigned_t TOPPERS_cfg_static_api_117 = 117;
#line 19 "../sample/tSample2.cfg"
const unsigned_t TOPPERS_cfg_valueof_domid_117 = (unsigned_t)(rDomain2);
#line 19 "../sample/tSample2.cfg"
const unsigned_t TOPPERS_cfg_valueof_twdord_117 = (unsigned_t)(2);
#line 19 "../sample/tSample2.cfg"
const unsigned_t TOPPERS_cfg_valueof_twdlen_117 = (unsigned_t)(3000);

#line 22 "../sample/tSample2.cfg"
const unsigned_t TOPPERS_cfg_static_api_118 = 118;

#line 23 "../sample/tSample2.cfg"
const unsigned_t TOPPERS_cfg_static_api_119 = 119;

#line 25 "../sample/tSample2.cfg"
const unsigned_t TOPPERS_cfg_static_api_120 = 120;

#line 26 "../sample/tSample2.cfg"
const unsigned_t TOPPERS_cfg_static_api_121 = 121;

#line 27 "../sample/tSample2.cfg"
const unsigned_t TOPPERS_cfg_static_api_122 = 122;

#line 28 "../sample/tSample2.cfg"
const unsigned_t TOPPERS_cfg_static_api_123 = 123;

