/* kernel_mem3.c */
#include "kernel/kernel_int.h"
#include "kernel_cfg.h"

#if !(TKERNEL_PRID == 0x0006U && (TKERNEL_PRVER & 0xf000U) == 0x3000U)
#error The kernel does not match this configuration file.
#endif

/*
 *  Include Directives
 */

#include "rza1.h"
#include "target_timer.h"
#include "tHRPSVCPlugin_sSysLogSVCBody_SysLog_eSysLog_factory.h"
#include "tHRPSVCPlugin_sSerialPortSVCBody_SerialPort1_eSerialPort_factory.h"
#include "extsvc_fncode.h"
#include "tTask_tecsgen.h"
#include "tTimeEventHandler_tecsgen.h"
#include "tISR_tecsgen.h"
#include "tCpuExceptionHandler_tecsgen.h"
#include "tInitializeRoutine_tecsgen.h"
#include "tTerminateRoutine_tecsgen.h"
#include "tTimeEventHandler.h"
#include "tTimeEventHandler.h"
#include "tSample2.h"

const uint_t _kernel_tnum_meminib = 21U;

void *const _kernel_memtop_table[21] = {
	(void *)(IO1_ADDR),
	(void *)(IO2_ADDR)
};

const MEMINIB _kernel_meminib_table[21] = {{ TA_NOEXS, 0U, 0U, 0U }};

const uint_t _kernel_tnum_datasec = 0U;

TOPPERS_EMPTY_LABEL(const DATASECINIB, _kernel_datasecinib_table);

const uint_t _kernel_tnum_bsssec = 2U;

const BSSSECINIB _kernel_bsssecinib_table[2] = {{ 0, 0 }};

#ifndef ARM_PAGE_TABLE_RATIO
#define ARM_PAGE_TABLE_RATIO	100
#endif /* ARM_PAGE_TABLE_RATIO */

#define ARM_PAGE_TABLE_NUM		(20 * ARM_PAGE_TABLE_RATIO / 100)

const uint32_t _kernel_section_table[TNUM_DOMID][ARM_SECTION_TABLE_ENTRY] __attribute__((aligned(ARM_SECTION_TABLE_ALIGN),section(".page_table"),nocommon)) = {{ 0U }};

const uint32_t _kernel_page_table[ARM_PAGE_TABLE_NUM][ARM_PAGE_TABLE_ENTRY] __attribute__((aligned(ARM_PAGE_TABLE_ALIGN),section(".page_table"),nocommon)) = {{ 0U }};

