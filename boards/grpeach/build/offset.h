/* offset.h */

#define TCB_p_tinib		8
#define TCB_p_dominib	12
#define TCB_svclevel	21
#define TCB_sp			36
#define TCB_pc			40
#define TINIB_domid		0
#define TINIB_exinf		8
#define TINIB_task		12
#define TINIB_sstksz	20
#define TINIB_sstk		24
#define TINIB_ustksz	28
#define TINIB_ustk		32
#define DOMINIB_domptn	0
#define DOMINIB_p_section_table	16
#define SVCINIB_svcrtn	0
#define SVCINIB_stksz	4
#define T_EXCINF_rundom	8
#define T_EXCINF_cpsr	48
