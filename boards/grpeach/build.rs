use std::process::Command;
use std::env;
use std::path::Path;

fn main() {
    let out_dir = env::var("OUT_DIR").unwrap();

    let toppers_hrp3_kernel_top = "~/toppers_hrp3_in_rust";

    let inc_dirs = [
        # mbed libraries
        &format!("-I{}/mbed/api", toppers_hrp3_kernel_top),
        &format!("-I{}/mbed/hal", toppers_hrp3_kernel_top),
        &format!("-I{}/mbed/targets", toppers_hrp3_kernel_top),
        &format!("-I{}/mbed/targets/cmsis", toppers_hrp3_kernel_top),
        &format!("-I{}/mbed/targets/cmsis/TARGET_RENESAS", toppers_hrp3_kernel_top),
        &format!("-I{}/mbed/targets/cmsis/TARGET_RENESAS/TARGET_RZ_A1H", toppers_hrp3_kernel_top),
        &format!("-I{}/mbed/targets/cmsis/TARGET_RENESAS/TARGET_RZ_A1H/inc", toppers_hrp3_kernel_top),
        &format!("-I{}/mbed/targets/cmsis/TARGET_RENESAS/TARGET_RZ_A1H/inc/iobitmasks", toppers_hrp3_kernel_top),
        &format!("-I{}/mbed/targets/cmsis/TARGET_RENESAS/TARGET_RZ_A1H/inc/iodefines", toppers_hrp3_kernel_top),
        &format!("-I{}/mbed/targets/cmsis/TARGET_RENESAS/TARGET_RZ_A1H/TOOLCHAIN_GCC_ARM", toppers_hrp3_kernel_top),
        &format!("-I{}/mbed/targets/cmsis/TOOLCHAIN_GCC", toppers_hrp3_kernel_top),
        &format!("-I{}/mbed/targets/cmsis/TOOLCHAIN_GCC/TARGET_CORTEX_A", toppers_hrp3_kernel_top),
        &format!("-I{}/mbed/targets/hal/TARGET_RENESAS/TARGET_RZ_A1H", toppers_hrp3_kernel_top),
        &format!("-I{}/mbed/targets/hal/TARGET_RENESAS/TARGET_RZ_A1H/TARGET_MBED_MBRZA1H", toppers_hrp3_kernel_top),

        # 
        &format!("-I{}/kernel/org", ${TOPPERSASP_INCLUDE_DIRS}
    	${ToppersASPnfOverlay_INCLUDE_DIRS}
	    ${TARGET_TOPPERS_COMMON_INCLUDE_DIRS}
    	${TARGET_TOPPERSASP_COMMON_INCLUDE_DIRS}

        "-Itoppers_hrp3_kernel/Inc"
    ];

    let defines = [
        "-DSTM32F103xB"
    ];

    let srcs = [
        /* mbed */
        /*
        [&format!("{}/mbed-lib", toppers_hrp3_kernel_top), "alarm.c"],
        [&format!("{}/mbed-lib", toppers_hrp3_kernel_top), "cyclic.c"],
        [&format!("{}/mbed-lib", toppers_hrp3_kernel_top), "dataqueue.c"],
        [&format!("{}/mbed-lib", toppers_hrp3_kernel_top), "domain.c"],
        [&format!("{}/mbed-lib", toppers_hrp3_kernel_top), "eventflag.c"],
        [&format!("{}/mbed-lib", toppers_hrp3_kernel_top), "exception.c"],
        [&format!("{}/mbed-lib", toppers_hrp3_kernel_top), "interrupt.c"],
        [&format!("{}/mbed-lib", toppers_hrp3_kernel_top), "mem_manage.c"],
        [&format!("{}/mbed-lib", toppers_hrp3_kernel_top), "memory.c"],
        [&format!("{}/mbed-lib", toppers_hrp3_kernel_top), "mempfix.c"],
        [&format!("{}/mbed-lib", toppers_hrp3_kernel_top), "messagebuf.c"],
        [&format!("{}/mbed-lib", toppers_hrp3_kernel_top), "mutex.c"],
        [&format!("{}/mbed-lib", toppers_hrp3_kernel_top), "pridataq.c"],
        [&format!("{}/mbed-lib", toppers_hrp3_kernel_top), "semaphore.c"],
        [&format!("{}/mbed-lib", toppers_hrp3_kernel_top), "startup.c"],
        [&format!("{}/mbed-lib", toppers_hrp3_kernel_top), "svc_table.c"],
        [&format!("{}/mbed-lib", toppers_hrp3_kernel_top), "sys_manage.c"],
        [&format!("{}/mbed-lib", toppers_hrp3_kernel_top), "task.c"],
        [&format!("{}/mbed-lib", toppers_hrp3_kernel_top), "task_manage.c"],
        [&format!("{}/mbed-lib", toppers_hrp3_kernel_top), "task_refer.c"],
        [&format!("{}/mbed-lib", toppers_hrp3_kernel_top), "task_sync.c"],
        [&format!("{}/mbed-lib", toppers_hrp3_kernel_top), "task_term.c"],
        [&format!("{}/mbed-lib", toppers_hrp3_kernel_top), "taskhook.c"],
        [&format!("{}/mbed-lib", toppers_hrp3_kernel_top), "time_event.c"],
        [&format!("{}/mbed-lib", toppers_hrp3_kernel_top), "time_manage.c"],
        [&format!("{}/mbed-lib", toppers_hrp3_kernel_top), "wait.c"],
        */
        // use libmbed.a
    ];

    let mut objs: Vec<String> = Vec::new();

    for src in &srcs {
        let obj = src[1].to_string().replace(".c", ".o");

        Command::new("arm-none-eabi-gcc")
            .arg("-c")
            .args(&["-mcpu=cortex-a9", "-mthumb", "-mfloat-abi=soft"])
            .args(&defines)
            .args(&inc_dirs)
            .arg(&format!("{}/{}",src[0], src[1]))
            .arg("-o")
            .arg(&format!("{}/{}", out_dir, obj))
            .status().unwrap();

        objs.push(obj);
    }

    Command::new("arm-none-eabi-as")
        .args(&["-mcpu=cortex-a9", "-mthumb", "-mfloat-abi=soft"])
        .args(&["../../arch/arm_gcc/common/start.S"])
        .args(&["-o"])
        .arg(&format!("{}/start.o", out_dir))
        .status().unwrap();

    Command::new("arm-none-eabi-ar")
        .args(&["crus", "libtoppers_hrp3_kernel.a"])
        .arg(&format!("{}/start.o", out_dir))
        .current_dir(&Path::new(&out_dir))
        .status().unwrap();

    println!("cargo:rustc-link-search=native={}", out_dir);
    println!("cargo:rustc-link-lib=static=toppers_hrp3_kernel");

    println!("cargo:rerun-if-changed=build.rs");
    println!("cargo:rerun-if-changed=target/gr_peach_gcc/.ld");
    println!("cargo:rerun-if-changed=../../arch/arm_gcc/common/start.S");
}